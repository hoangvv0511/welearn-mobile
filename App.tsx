import * as React from 'react';
import { Provider } from 'react-redux';
import { StyleSheet, View } from 'react-native';
import App from './src/app.component';
import store from './src/core/redux/store';
import codePush from 'react-native-code-push';
import { ApplicationProvider } from '@ui-kitten/components';
import { mapping, light as lightTheme } from '@eva-design/eva';

class WeLearnApp extends React.Component {
  render() {
    return (
      <ApplicationProvider mapping={mapping} theme={lightTheme}>
        <Provider store={store}>
          <View style={styles.root}>
            <App />
          </View>
        </Provider>
      </ApplicationProvider>
    );
  }
}

let codePushOptions = {
  checkFrequency: codePush.CheckFrequency.MANUAL
};

export default codePush(codePushOptions)(WeLearnApp);;

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: '#FFF',
  },
});
