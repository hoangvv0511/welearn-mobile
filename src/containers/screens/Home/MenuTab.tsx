import { createAppContainer } from 'react-navigation';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs'
import Exam from '../../../containers/screens/Home/Exam';
import OnlineRoom from '../../../containers/screens/Home/OnlineRoom';
import { PINK_BLUR, RED_BOLD } from '../../../core/config/color';

const examTabName = 'Ôn tập'
const onlineRoom = 'Phòng thi online'

const MainTabNavigator = createMaterialTopTabNavigator({
  [examTabName]: Exam,
  [onlineRoom]: OnlineRoom
}, {
  tabBarOptions: {
    activeTintColor: '#FFF',
    inactiveTintColor: PINK_BLUR,
    style: {
      backgroundColor: RED_BOLD,
    },
    showLabel: true,
    upperCaseLabel: false,
    tabStyle: {
      justifyContent: 'center',
      alignItems: 'center',
      alignContent: 'center'
    },
    labelStyle: {
      fontSize: 14,
      fontFamily: 'Roboto-Bold'
    },
    indicatorStyle: {
      backgroundColor: '#FFF',
    },
  },
  swipeEnabled: true,
  lazy: true
})

export default createAppContainer(MainTabNavigator);