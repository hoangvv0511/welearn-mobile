import { OverflowMenu } from '@ui-kitten/components'
import { PopoverPlacements } from '@ui-kitten/components/ui/popover/type'
import React, { Component } from 'react'
import { ScrollView, StyleSheet, Text, TextInput, View } from 'react-native'
import ActionButton from 'react-native-action-button'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { default as FontAwesome5Icon, default as Icon } from 'react-native-vector-icons/FontAwesome5Pro'
import { connect } from 'react-redux'
import { RED_BOLD, WHITE } from '../../../core/config/color'
import { subjectMenu } from '../../../core/libs/constant/subject'
import Header from '../../components/Header'
import FilterModal, { ExamFilter, SystemRoomFilter } from './components/FilterModal'
import MainTabNavigator from './MenuTab'

const searchIcon = {
  name: 'search',
  size: 19,
  color: '#a94853'
}

class Home extends Component {

  static router = MainTabNavigator.router
  tabRef = React.createRef()
  filterModal = React.createRef<FilterModal>()

  state = {
    menuVisible: false,
    selectedIndex: 0,
    filter: this.props.navigation.state.index === 0 ? ExamFilter[5] : SystemRoomFilter[4],
    searchStr: '',
    searchValue: '',
    isSubmit: false
  }

  setSelectedIndex = (index) => this.setState({ selectedIndex: index })
  setMenuVisible = (visible) => this.setState({ menuVisible: visible })

  onItemSelect = (index) => {
    this.setSelectedIndex(index);
    this.setMenuVisible(false);
  };

  toggleMenu = () => {
    this.setMenuVisible(!this.state.menuVisible);
  };

  filterButton = () => {
    return (
      <ActionButton
        buttonColor={RED_BOLD}
        style={{ right: -10 }}
        onPress={() => this.filterModal.current.open()}
        renderIcon={() => <FontAwesome5Icon solid name={'filter'} color={WHITE} size={20} />}
      />
    )
  }

  onFilterChange = (filter) => this.setState({ filter })

  header = () => {
    const searchBar = () => {
      const { name, size, color } = searchIcon
      const { searchStr } = this.state

      return (
        <View style={styles.searchBarContainer}>
          <View style={styles.searchBarBody}>
            <View style={styles.searchIcon} >
              <Icon name={name} size={size} color={color} />
            </View>
            <View style={styles.inputContainer} >
              <TextInput
                style={styles.input}
                placeholder={'Nhập từ khóa để tìm...'}
                value={searchStr}
                onChangeText={text => this.setState({ searchStr: text })}
                onSubmitEditing={() => this.setState({ searchValue: this.state.searchStr })}
                returnKeyType={'search'}
                placeholderTextColor={'#a94853'}
                selectionColor={'#0040FF'}
                underlineColorAndroid={'transparent'}
              />
            </View>
          </View>
        </View>
      )
    }

    const menuBar = () => {
      const { menuVisible, selectedIndex } = this.state

      return (
        <View style={styles.menuContainer}>
          <OverflowMenu
            backdropStyle={styles.backdrop}
            data={subjectMenu}
            visible={menuVisible}
            selectedIndex={selectedIndex}
            onSelect={this.onItemSelect}
            onBackdropPress={this.toggleMenu}
            placement={PopoverPlacements.BOTTOM}
          >
            <TouchableOpacity onPress={this.toggleMenu}>
              <Text style={styles.menuButtonText}>{subjectMenu[selectedIndex].title}</Text>
            </TouchableOpacity>
          </OverflowMenu>
        </View>
      )
    }

    return (
      <Header
        rightView={menuBar}
        middleView={searchBar}
        navigation={this.props.navigation}
      />
    )
  }

  tab = () => {
    return (
      <MainTabNavigator
        screenProps={{ ...this.props, ...this.state }}
        navigation={this.props.navigation}
      />
    )
  }

  render() {
    return (
      <View style={styles.root}>
        <ScrollView style={styles.root} contentContainerStyle={styles.root} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps={'never'}>
          {this.header()}
          {this.tab()}
        </ScrollView>
        <FilterModal
          ref={this.filterModal}
          screenId={this.props.navigation.state.index}
          onPress={this.onFilterChange}
          screenFilter={this.state.filter}
        />
        {this.filterButton()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1
  },
  searchBarContainer: {
    flex: 0.63,
  },
  searchBarBody: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#8b0616',
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10
  },
  menuContainer: {
    flex: 0.22,
    justifyContent: 'center',
    alignItems: 'center'
  },
  container: {
    minHeight: 256,
  },
  backdrop: {
    backgroundColor: 'transparent',
  },
  menuButtonText: {
    color: '#FFF',
    fontSize: 14,
    fontFamily: 'OpenSans-Bold',
    marginRight: 5
  },
  input: {
    color: '#FFF',
    fontSize: 15,
    fontFamily: 'OpenSans-light',
  },
  searchIcon: {
    flex: 0.15,
    justifyContent: 'center',
    alignItems: 'center'
  },
  inputContainer: {
    flex: 0.8
  }
})

const mapStateToProps = state => {
  return {
    user: state.System.user
  }
}

export default connect(mapStateToProps)(Home)