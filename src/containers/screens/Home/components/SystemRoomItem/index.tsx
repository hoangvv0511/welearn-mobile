import { Avatar } from '@ui-kitten/components'
import moment from 'moment'
import React, { PureComponent } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { RED_BOLD, TEXT_REGULAR } from '../../../../../core/config/color'
import { getSubjectImage, Subject } from '../../../../../core/libs/constant/subject'
import Card from '../../../../components/Card'
import IconWithText from '../../../../components/IconWithText'

class ExamItemProps {
  id: number
  name: string
  exam_id: number
  start_time: number
  create_at: number
  systemRoom_exam: {
    name: string
    subject_id: string
    exam_time: number
    number_of_question: number
  }
  onPress: () => void
}

export const NOT_OPEN = 'Chưa mở'
export const CLOSE = 'Đã đóng'
export const TESTING = 'Đang thi'

export const getSystemStatus = (startTime: number, endTime: number, currentTime: number) => {

  if (currentTime < startTime) {
    return { text: NOT_OPEN, color: 'red', showTime: true }
  } else if (currentTime <= endTime && currentTime >= startTime) {
    return { text: TESTING, color: 'green', showTime: false }
  } else {
    return { text: CLOSE, color: 'blue', showTime: false }
  }
}

class SystemRoomItem extends PureComponent<ExamItemProps> {

  state = {
    timeLeft: null
  }

  renderStatus = () => {
    const {
      start_time, systemRoom_exam: { exam_time }
    } = this.props
    const endTime = start_time + exam_time * 60 * 1000
    const currentTime = new Date().getTime()
    const currentStatus = getSystemStatus(start_time, endTime, currentTime)

    return (
      <View style={styles.statusContainer}>
        <Text style={styles.timeText}>
          Trạng thái: <Text style={[styles.statusText, { color: currentStatus.color }]}>{currentStatus.text}</Text>
        </Text>
        {currentStatus.showTime ?
          <Text style={styles.statusText}>{`  (${moment(start_time).format('LLL')})`}</Text>
          : null}
      </View>
    )
  }

  render() {
    const {
      id, name, onPress, create_at, start_time, systemRoom_exam: { subject_id, exam_time, number_of_question }
    } = this.props
    const createAt = moment(create_at).format('DD-MM-YYYY')
    const subjectName = Subject[subject_id]
    const subjectImage = getSubjectImage(subject_id)

    return (
      <TouchableOpacity activeOpacity={0.4} onPress={onPress}>
        <Card key={id}>
          <View style={styles.itemRoot}>
            <View style={styles.timeAndSubject}>
              <Text style={styles.timeText}>{createAt}</Text>
              <View style={styles.subjectAndImage}>
                <Text style={styles.subjectText}>{subjectName}</Text>
                <Avatar size={'tiny'} source={subjectImage} />
              </View>
            </View>
            <View style={styles.examInfo}>
              <View style={styles.bigSubjectImage}>
                <Avatar size={'medium'} source={subjectImage} />
              </View>
              <View style={styles.infoContainer}>
                <Text style={styles.titleText}>{name}</Text>
                <View style={styles.oneLineInfo}>
                  <IconWithText
                    regular
                    iconName={'alarm-clock'}
                    iconSize={13}
                    iconColor={RED_BOLD}
                    text={`${exam_time} phút`}
                    textStyle={[styles.timeText, { color: RED_BOLD }]}
                  />
                  <IconWithText
                    regular
                    rootStyle={{ marginLeft: 10 }}
                    iconName={'question-circle'}
                    iconSize={13}
                    iconColor={RED_BOLD}
                    text={`${number_of_question} câu`}
                    textStyle={[styles.timeText, { color: RED_BOLD }]}
                  />
                </View>
              </View>
            </View>
            <View style={styles.description}>
              <Text style={styles.bodyText}>{name}</Text>
            </View>
            <View style={styles.review}>
              <View style={styles.line}></View>
              <View style={styles.reviewInfo}>
                {this.renderStatus()}
              </View>
            </View>
          </View>
        </Card>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  itemRoot: {
    flex: 1,
    margin: 10
  },
  timeAndSubject: {
    flex: 0.15,
    marginTop: -5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  subjectAndImage: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  timeText: {
    fontSize: 11,
    fontFamily: 'OpenSans-Regular',
    color: TEXT_REGULAR
  },
  statusContainer: {
    flexDirection: 'row',
    marginLeft: 10,
    marginTop: 5
  },
  subjectText: {
    fontSize: 14,
    fontFamily: 'OpenSans-Bold',
    color: TEXT_REGULAR,
    marginRight: 10
  },
  examInfo: {
    flex: 0.35,
    marginTop: 3,
    flexDirection: 'row',
  },
  bigSubjectImage: {
    flex: 0.15,
    justifyContent: 'center',
    alignItems: 'center'
  },
  infoContainer: {
    flex: 0.85,
    marginLeft: 15,
    marginRight: 15
  },
  description: {
    flex: 0.3,
    marginLeft: 5
  },
  titleText: {
    color: RED_BOLD,
    fontSize: 15,
    fontFamily: 'OpenSans-Bold',
  },
  statusText: {
    fontSize: 13,
    fontFamily: 'OpenSans-Bold',
  },
  bodyText: {
    color: TEXT_REGULAR,
    fontSize: 15,
    fontFamily: 'OpenSans-Regular',
  },
  oneLineInfo: {
    flexDirection: 'row',
    marginTop: 5
  },
  newImageView: {
    marginLeft: 10
  },
  newImage: {
    width: 23,
    height: 23
  },
  line: {
    height: 0.5,
    backgroundColor: '#e6e6e6',
    marginHorizontal: 10
  },
  review: {
    flex: 0.2,
    marginBottom: 0
  },
  reviewInfo: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  }
})

export default SystemRoomItem
