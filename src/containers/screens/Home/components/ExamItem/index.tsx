import React, { FunctionComponent, PureComponent } from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import Card from '../../../../components/Card'
import { Avatar } from '@ui-kitten/components'
import { TEXT_REGULAR, RED_BOLD } from '../../../../../core/config/color'
import IconWithText from '../../../../components/IconWithText'
import moment from 'moment'
import { Subject, getSubjectImage } from '../../../../../core/libs/constant/subject'
import { TouchableOpacity } from 'react-native-gesture-handler'

class ExamItemProps {
  id: number
  name: string
  subject_id: string
  question: string
  anwser: string
  exam_time: number
  number_of_question: number
  is_online: boolean
  year: number
  create_at: number
  start_id: number
  count: number = 0
  star: number = 0
  is_tested: boolean
  is_rated: boolean
  onPress: () => void
}

class ExamItem extends PureComponent<ExamItemProps> {
  render() {
    const {
      id, name, subject_id, number_of_question, exam_time, create_at, count, star, is_tested, onPress
    } = this.props
    const createAt = moment(create_at).format('DD-MM-YYYY')
    const subjectName = Subject[subject_id]
    const subjectImage = getSubjectImage(subject_id)
    const newDate = new Date().getTime()
    const createDate = moment(create_at).toDate().getTime()
    const showNewLabel = Math.ceil((newDate - createDate) / (1000 * 60 * 60 * 24)) <= 7

    return (
      <TouchableOpacity activeOpacity={0.4} onPress={onPress}>
        <Card key={id}>
          <View style={styles.itemRoot}>
            <View style={styles.timeAndSubject}>
              <Text style={styles.timeText}>{createAt}</Text>
              <View style={styles.subjectAndImage}>
                <Text style={styles.subjectText}>{subjectName}</Text>
                <Avatar size={'tiny'} source={subjectImage} />
              </View>
            </View>
            <View style={styles.examInfo}>
              <View style={styles.bigSubjectImage}>
                <Avatar size={'medium'} source={subjectImage} />
              </View>
              <View style={styles.infoContainer}>
                <Text style={styles.titleText}>{name}</Text>
                <View style={styles.oneLineInfo}>
                  <IconWithText
                    regular
                    iconName={'alarm-clock'}
                    iconSize={13}
                    iconColor={RED_BOLD}
                    text={`${exam_time} phút`}
                    textStyle={[styles.timeText, { color: RED_BOLD }]}
                  />
                  <IconWithText
                    regular
                    rootStyle={{ marginLeft: 10 }}
                    iconName={'question-circle'}
                    iconSize={13}
                    iconColor={RED_BOLD}
                    text={`${number_of_question} câu`}
                    textStyle={[styles.timeText, { color: RED_BOLD }]}
                  />
                  {is_tested && <IconWithText
                    regular
                    rootStyle={{ marginLeft: 10 }}
                    iconName={'flag-checkered'}
                    iconSize={13}
                    iconColor={'#4caf4f'}
                    text={'Đã làm'}
                    textStyle={[styles.timeText, { color: '#4caf4f' }]}
                  />}
                </View>
              </View>
            </View>
            <View style={styles.description}>
              <Text style={styles.bodyText}>{name}</Text>
            </View>
            <View style={styles.review}>
              <View style={styles.line}></View>
              <View style={styles.reviewInfo}>
                <Text style={[styles.timeText, { marginLeft: 10 }]}>{star} bình chọn</Text>
                <Text style={[styles.timeText, { marginLeft: 10 }]}>{count} lượt thi</Text>
              </View>
            </View>
          </View>
        </Card>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  itemRoot: {
    flex: 1,
    margin: 10
  },
  timeAndSubject: {
    flex: 0.15,
    marginTop: -5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  subjectAndImage: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  timeText: {
    fontSize: 11,
    fontFamily: 'OpenSans-Regular',
    color: TEXT_REGULAR
  },
  subjectText: {
    fontSize: 14,
    fontFamily: 'OpenSans-Bold',
    color: TEXT_REGULAR,
    marginRight: 10
  },
  examInfo: {
    flex: 0.35,
    marginTop: 3,
    flexDirection: 'row',
  },
  bigSubjectImage: {
    flex: 0.15,
    justifyContent: 'center',
    alignItems: 'center'
  },
  infoContainer: {
    flex: 0.85,
    marginLeft: 15,
    marginRight: 15
  },
  description: {
    flex: 0.3,
    marginLeft: 5
  },
  titleText: {
    color: RED_BOLD,
    fontSize: 15,
    fontFamily: 'OpenSans-Bold',
  },
  bodyText: {
    color: TEXT_REGULAR,
    fontSize: 15,
    fontFamily: 'OpenSans-Regular',
  },
  oneLineInfo: {
    flexDirection: 'row',
    marginTop: 5
  },
  newImageView: {
    marginLeft: 10
  },
  newImage: {
    width: 23,
    height: 23
  },
  line: {
    height: 0.5,
    backgroundColor: '#e6e6e6',
    marginHorizontal: 10
  },
  review: {
    flex: 0.2,
    marginBottom: 0
  },
  reviewInfo: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  }
})

export default ExamItem
