import React, { FunctionComponent } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, ViewStyle } from 'react-native'
import { RED_BOLD } from '../../../../../core/config/color'

interface Props {
  text: string
  onPress(): void
  backgroundColor: string
  rootStyle: ViewStyle
}

const RoundButton: FunctionComponent<Props> = props => {
  const { text, onPress, backgroundColor, rootStyle } = props

  return (
    <TouchableOpacity activeOpacity={0.6} style={[styles.commonView, { backgroundColor: backgroundColor }, rootStyle]} onPress={onPress}>
      <View style={styles.commonViewBody}>
        <Text style={styles.commonViewText}>{text}</Text>
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  commonView: {
    height: 35,
    borderRadius: 20,
    backgroundColor: 'white',
    shadowColor: '#FFF',
    elevation: 1,
    shadowOffset: {
      width: 0,
      height: 2
    },
    margin: 5
  },
  commonViewText: {
    fontSize: 14,
    fontFamily: 'OpenSans-Bold',
    color: RED_BOLD,
    margin: 10
  },
  commonViewBody: {
    flex: 1,
    marginHorizontal: 10,
    marginVertical: 12,
    justifyContent: 'center',
    alignItems: 'center',
  },
})

export default RoundButton
