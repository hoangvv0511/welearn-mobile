import React, { PureComponent } from 'react'
import { StyleSheet, View } from 'react-native'
import Modal from 'react-native-modal'
import { RED_BOLD, WHITE } from '../../../../../core/config/color'
import RoundButton from '../RoundButton'

interface ModalProps {
  screenId: number
  onPress(item: any): void
  screenFilter: string
}

export const ExamFilter = [
  'Bình chọn nhiều nhất',
  'Mới nhất',
  'Làm nhiều nhất',
  'Đã làm',
  'Chưa làm',
  'Bỏ lọc'
]

export const SystemRoomFilter = [
  'Đang thi',
  'Mới nhất',
  'Chưa mở',
  'Bỏ lọc'
]

export enum ExamEnumFilter {
  'Bình chọn nhiều nhất' = '_sort=star',
  'Mới nhất' = '_sort=create_at',
  'Làm nhiều nhất' = '_sort=count',
  'Đã làm' = 'is_tested=true',
  'Chưa làm' = 'is_tested=false',
  'Bỏ lọc' = '_sort=id',
}


export enum ExamEnumSystemRoom {
  'Đang thi' = 'is_open=openning',
  'Mới nhất' = '_sort=create_at',
  'Chưa mở' = 'is_open=not_open',
  'Bỏ lọc' = '_sort=id'
}

class FilterModal extends PureComponent<ModalProps> {

  state = {
    isVisible: false
  }

  open = () => this.setState({ isVisible: true })
  close = () => this.setState({ isVisible: false })

  render() {
    const { isVisible } = this.state
    const { screenId, onPress, screenFilter } = this.props
    const currentFilter = screenId === 0 ? ExamFilter : SystemRoomFilter

    return (
      <Modal
        isVisible={isVisible}
        backdropColor={'#757575'}
        style={styles.rootModal}
        onBackButtonPress={this.close}
        onBackdropPress={this.close}
        swipeDirection={'down'}
        onSwipeComplete={this.close}
      >
        <View style={styles.modalBodyRoot}>
          <View style={styles.content}>
            {
              currentFilter.map((filter, index) => {
                return (
                  <RoundButton
                    text={filter}
                    onPress={() => {
                      this.close()
                      onPress && onPress(filter)
                    }} 
                    backgroundColor={screenFilter === filter ? 'yellow' : WHITE}
                    />
                )
              })
            }
          </View>
        </View>
      </Modal >
    )
  }
}

const styles = StyleSheet.create({
  rootModal: {
    flex: 1,
    justifyContent: 'flex-end',
    margin: 0
  },
  modalBodyRoot: {
    // flex: 0.5,
    backgroundColor: RED_BOLD,
  },
  titleText: {
    fontSize: 14,
    fontFamily: 'Roboto-Regular',
    color: '#7F7F7F'
  },
  content: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 15,
    marginRight: 15,
    marginVertical: 10,
    flexWrap: 'wrap'
  }
})

export default FilterModal