import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { withNavigationFocus } from 'react-navigation';
import { connect } from 'react-redux';
import { BACKGROUND_COMMON } from '../../../../core/config/color';
import { SubjectReverse } from '../../../../core/libs/constant/subject';
import List from '../../../components/List';
import ExamItem from '../components/ExamItem';
import { ExamEnumFilter } from '../components/FilterModal';

class Exam extends Component {

  // shouldComponentUpdate = (nextProps) => {
  //   if (nextProps.isFocused !== this.props.isFocused && nextProps.isFocused && this.list
  //   ) {
  //     this.list.initialDataCall();
  //   }

  //   return true;
  // }

  render() {
    const { filter, selectedIndex, searchValue } = this.props.screenProps
    const { user } = this.props
    const subject = SubjectReverse[selectedIndex]
    const subjectFilter = subject ? `&subject_id=${subject}` : ''
    const search = searchValue.length ? `&name=${searchValue}` : ''

    return (
      <View style={styles.root}>
        <List
          ref={ref => this.list = ref}
          url={(`/exam?_order=DESC&user_id=${user.id}&is_online=false&${ExamEnumFilter[filter]}${subjectFilter}${search}`).trim()}
          renderItem={this.listItem}
        />
      </View>
    )
  }


  listItem = ({ item, index }) => {
    const { navigate } = this.props.navigation

    return (
      <ExamItem
        {...item}
        onPress={() => navigate('Testing', { examData: item, onBack: this.list.initialDataCall })}
      />
    );
  };
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: BACKGROUND_COMMON
  }
})

const mapStateToProps = state => {
  return {
    user: state.System.user
  }
}

export default connect(mapStateToProps)(withNavigationFocus(Exam))