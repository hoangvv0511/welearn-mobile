import React, { Component } from 'react'
import { Text, View, StyleSheet, ScrollView, RefreshControl, LayoutAnimation } from 'react-native'
import { authorizedHttp } from '../../../../../core/services/api'
import { connect } from 'react-redux'
import Header from '../../../../components/Header'
import { WHITE, RED_BOLD, TEXT_REGULAR } from '../../../../../core/config/color'
import { Subject } from '../../../../../core/libs/constant/subject'
import { TouchableOpacity } from 'react-native-gesture-handler'
import _ from 'lodash'
import { getSystemStatus, NOT_OPEN, CLOSE } from '../../components/SystemRoomItem'
import { withNavigationFocus, FlatList } from 'react-navigation'
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5Pro'
import { Avatar } from '@ui-kitten/components'
import moment from 'moment'

const getSystemRoom = (roomId: number, userId: string) => {
  return `systemRoom/${roomId}?user_id=${userId}`
}

const getTopUser = (exam_id: number, startTime: number, examTime: number) => {
  return `getTopUser/?exam_id=${exam_id}&start_time=${startTime}&exam_time=${examTime}`
}

const getUserResult = (exam_id: number, startTime: number, examTime: number, userId: string) => {
  return `history/?user_id=${userId}&exam_id=${exam_id}$dateFrom=${startTime}$dateTo=${examTime}`
}

class SystemRoomDetail extends Component {

  constructor(props) {
    super(props)

    this.state = {
      isLoading: false,
      topUser: [],
      roomData: null,
      userResult: [],
      showingMyResult: true,
      showingUserResult: true
    }
  }

  componentDidMount = () => {
    this.fetchData()
  }

  shouldComponentUpdate({ isFocused }) {
    if (isFocused !== this.props.isFocused && isFocused) {
      this.fetchData()
    }

    return true;
  }

  fetchData = () => {
    const { navigation, user } = this.props
    const { state: { params: { systemData: { id, start_time, systemRoom_exam, exam_id } } }
    } = navigation

    this.setState({ isLoading: true }, () => {
      Promise.all([
        authorizedHttp().get(getSystemRoom(id, user.id)),
        authorizedHttp().get(getTopUser(exam_id, start_time, systemRoom_exam.exam_time)),
        authorizedHttp().get(getUserResult(exam_id, start_time, start_time + systemRoom_exam.exam_time * 60 * 1000, user.id))
      ])
        .then(res => {
          this.setState({
            roomData: res[0].data,
            topUser: res[1].data,
            userResult: res[2].data
          }, () => this.setState({ isLoading: false }))
        })
        .catch(x => this.setState({ isLoading: false }))
    })
  }

  oneLine = (label: string, text: string) => {
    return (
      <View style={{ flexDirection: 'row', marginHorizontal: 10, marginVertical: 5 }}>
        <Text style={styles.oneLineLabel}>{label}</Text>
        <Text style={styles.oneLineText}>{text}</Text>
      </View>
    )
  }

  roomInfoView = () => {
    const { isLoading, roomData } = this.state

    if (isLoading || !roomData) {
      return null
    }

    const {
      name,
      start_time,
      systemRoom_exam: {
        exam_time,
        number_of_question,
        subject_id
      }
    } = roomData

    return (
      <View style={styles.roomInfoContainer}>
        <Text style={styles.bigTitleText}>{name}</Text>
        <View style={{ marginTop: 5 }}>
          {this.oneLine('- Môn thi :' + ' '.repeat(2), Subject[subject_id])}
          {this.oneLine('- Thời gian :' + ' '.repeat(2), `${exam_time} phút`)}
          {this.oneLine('- Số câu hỏi :' + ' '.repeat(2), `${number_of_question} câu`)}
          {this.oneLine('- Thời gian mở phòng :' + ' '.repeat(2), moment(start_time).format('DD-MM-YYYY') + ' lúc ' + moment(start_time).format('HH:mm'))}
        </View>
        <View>
          {this.oneLine('- Những lưu ý khi thi online :' + ' '.repeat(2), ' ')}
          <Text style={styles.noteText}>👉 Cố gắng làm bài thi trong thời gian quy định</Text>
          <Text style={styles.noteText}>👉 Không xem tài liệu</Text>
          <Text style={styles.noteText}>👉 Không được thi nếu vào trễ quá nửa thời gian thi</Text>
          <Text style={styles.noteText}>👉 Thí sinh chỉ có thể thi 1 lần</Text>
          <Text style={styles.noteText}>👉 Kết quả phòng thi sẽ được lưu trong 3 ngày, sau thời gian này, phòng thi sẽ được hệ thống tự động xóa</Text>
        </View>
      </View>
    )
  }

  listTopUsersAndMyResult = () => {
    const { isLoading, roomData, showingMyResult, showingUserResult, topUser } = this.state

    if (isLoading || !roomData) return null

    const { user } = this.props
    const { userResult } = this.state
    const point = userResult.length ? userResult[0].point.toFixed(2) : ''
    const time = userResult.length ? moment.utc(userResult[0].finish_time * 1000).format('HH:mm:ss') : ''

    return (
      <View style={styles.resultContainer}>
        <View>
          <TouchableOpacity style={styles.listBtn} onPress={this.handleShowMyResult}>
            <Text style={styles.listBtnText}>Kết quả của tôi</Text>
            <FontAwesome5Icon name={!showingMyResult ? 'angle-down' : 'angle-up'} size={25} color={RED_BOLD} style={styles.icon} />
          </TouchableOpacity>
          {showingMyResult ? <View>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ flex: 0.5, alignItems: 'center' }}>
                <Text style={styles.tableLabelText}>Thông tin</Text>
                <View style={styles.avatarContainer}>
                  <Avatar
                    shape='round'
                    source={{ uri: user.image }}
                    size={'small'} />
                  <Text style={styles.nameText} numberOfLines={2}>{user.name}</Text>
                </View>
              </View>
              <View style={{ flex: 0.2, alignItems: 'center' }}>
                <Text style={styles.tableLabelText}>Điểm số</Text>
                <Text style={[styles.nameText, { marginTop: 7, marginLeft: 0 }]}>{point}</Text>
              </View>
              <View style={{ flex: 0.3, alignItems: 'center' }}>
                <Text style={styles.tableLabelText}>Thời gian</Text>
                <Text style={[styles.nameText, { marginTop: 7, marginLeft: 0 }]}>{time}</Text>
              </View>
            </View>
          </View> : null}
        </View>
        <View style={{ marginTop: 10 }}>
          <TouchableOpacity style={styles.listBtn} onPress={this.handleShowUserResult}>
            <Text style={styles.listBtnText}>Top 50 điểm cao</Text>
            <FontAwesome5Icon name={!showingUserResult ? 'angle-down' : 'angle-up'} size={25} color={RED_BOLD} style={styles.icon} />
          </TouchableOpacity>
          {showingUserResult ? <View>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ flex: 0.1, alignItems: 'center' }}>
                <Text style={styles.tableLabelText}>STT</Text>
              </View>
              <View style={{ flex: 0.4, alignItems: 'center' }}>
                <Text style={styles.tableLabelText}>Thông tin</Text>
              </View>
              <View style={{ flex: 0.2, alignItems: 'center' }}>
                <Text style={styles.tableLabelText}>Điểm số</Text>
              </View>
              <View style={{ flex: 0.3, alignItems: 'center' }}>
                <Text style={styles.tableLabelText}>Thời gian</Text>
              </View>
            </View>
            {
              topUser.map((item, index) => {
                return (
                  <View style={{ flexDirection: 'row', marginVertical: 10 }}>
                    <View style={{ flex: 0.1, alignItems: 'center' }}>
                      <Text style={[styles.nameText, { marginTop: 7, marginLeft: 0 }]}>{`${index + 1}`}</Text>
                    </View>
                    <View style={{ flex: 0.4, alignItems: 'center' }}>
                      <View style={styles.avatarContainer}>
                        <Avatar
                          shape='round'
                          source={{ uri: item.history_user.image }}
                          size={'small'} />
                        <Text style={styles.nameText} numberOfLines={2}>{item.history_user.name}</Text>
                      </View>
                    </View>
                    <View style={{ flex: 0.2, alignItems: 'center' }}>
                      <Text style={[styles.nameText, { marginTop: 7, marginLeft: 0 }]}>{item.point.toFixed(2)}</Text>
                    </View>
                    <View style={{ flex: 0.3, alignItems: 'center' }}>
                      <Text style={[styles.nameText, { marginTop: 7, marginLeft: 0 }]}>{moment.utc(item.finish_time * 1000).format('HH:mm:ss')}</Text>
                    </View>
                  </View>
                )
              })
            }
          </View> : null}
        </View>
      </View>
    )
  }

  handleShowMyResult = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    this.setState({ showingMyResult: !this.state.showingMyResult })
  }

  handleShowUserResult = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    this.setState({ showingUserResult: !this.state.showingUserResult })
  }

  startButton = () => {
    const { isLoading, roomData } = this.state

    if (isLoading || !roomData) return null

    const { is_tested, start_time, systemRoom_exam: { exam_time } } = this.state.roomData

    if (is_tested) return null

    const endTime = start_time + exam_time * 60 * 1000
    const currentTime = new Date().getTime()
    const currentStatus = getSystemStatus(start_time, endTime, currentTime)

    if (currentStatus.text === NOT_OPEN || currentStatus.text === CLOSE) return null

    if (start_time + ((exam_time * 60 * 1000) / 2) - currentTime < 0) return null

    return (
      <TouchableOpacity style={styles.startButtonView} onPress={this.handleStartExam}>
        <Text style={styles.startButtonText}>Bắt đầu làm bài</Text>
      </TouchableOpacity>
    )
  }

  handleStartExam = () => {
    const { systemRoom_exam, is_tested, is_rated, array_answer, array_thematic, array_difficulty } = this.state.roomData
    const { navigate } = this.props.navigation
    const endTime = this.state.roomData.start_time + systemRoom_exam.exam_time * 60 * 1000

    const examData = {
      ...systemRoom_exam,
      is_tested,
      is_rated,
      array_answer,
      array_thematic,
      array_difficulty
    }

    navigate('Testing', { examData, disabledTestAgain: true, endTime })
  }

  content = () => {
    return (
      <View style={{ flex: 1 }}>
        {this.roomInfoView()}
        {this.listTopUsersAndMyResult()}
      </View>
    )
  }

  middleViewHeader = () => {
    const { systemData: { id } } = this.props.navigation.state.params

    return (
      <View style={styles.headerView}>
        <Text style={styles.headerText}>{`Phòng #${id}`}</Text>
      </View>
    )
  }

  refreshControl = () => {
    const { isLoading } = this.state;
    return <RefreshControl refreshing={isLoading} onRefresh={this.fetchData} />;
  }

  render() {
    const { systemData } = this.props.navigation.state.params

    return (
      <>
        <Header
          middleView={this.middleViewHeader}
          back
          navigation={this.props.navigation}
        />
        <ScrollView
          style={styles.roomContainer}
          showsVerticalScrollIndicator={false}
          refreshControl={this.refreshControl()}
        >
          {this.content()}
        </ScrollView>
        {this.startButton()}
      </>
    )
  }
}

const styles = StyleSheet.create({
  avatarContainer: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
  },
  userInfoBody: {
    flex: 1,
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  headerText: {
    fontSize: 18,
    color: WHITE,
    fontFamily: 'Roboto-Bold',
    marginRight: 45
  },
  headerView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  roomContainer: {
    flex: 1,
  },
  roomInfoContainer: {
    marginTop: 10
  },
  bigTitleText: {
    fontSize: 20,
    color: RED_BOLD,
    fontFamily: 'OpenSans-Bold',
    textAlign: 'center'
  },
  bigTitleView: {
    alignItems: 'center'
  },
  oneLineLabel: {
    fontSize: 16,
    color: TEXT_REGULAR,
    fontFamily: 'OpenSans-Bold',
  },
  oneLineText: {
    fontSize: 16,
    color: RED_BOLD,
    fontFamily: 'OpenSans-SemiBold',
  },
  startButtonText: {
    fontSize: 20,
    color: WHITE,
    fontFamily: 'OpenSans-Bold',
  },
  startButtonView: {
    height: 55,
    marginBottom: 0,
    backgroundColor: '#288CDC',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  listBtn: {
    height: 60,
    backgroundColor: '#F2F2F2',
    flexDirection: 'row',
    alignItems: 'center'
  },
  listBtnText: {
    fontSize: 16,
    color: RED_BOLD,
    fontFamily: 'OpenSans-Bold',
    marginLeft: 10
  },
  noteText: {
    fontSize: 15,
    color: RED_BOLD,
    fontFamily: 'OpenSans-SemiBoldItalic',
    marginHorizontal: 10,
    marginVertical: 3
  },
  icon: {
    position: 'absolute',
    right: 15
  },
  resultContainer: {
    marginTop: 10
  },
  tableLabelText: {
    fontSize: 13,
    marginVertical: 10,
    fontFamily: 'OpenSans-Bold',
  },
  nameText: {
    fontSize: 14,
    color: RED_BOLD,
    fontFamily: 'OpenSans-Bold',
    marginLeft: 10
  }
})

const mapStateToProps = state => {
  return {
    user: state.System.user
  }
}

export default connect(mapStateToProps)(withNavigationFocus(SystemRoomDetail))