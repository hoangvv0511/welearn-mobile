import * as React from 'react';
import {
  Text,
  TouchableOpacity,
  View,
  StyleSheet,
  Image,
  FlatList,
  TouchableHighlight,
  Dimensions
} from 'react-native';
import FontAwesome5Pro from 'react-native-vector-icons/FontAwesome5Pro';
import { RED_BOLD, WHITE, ORANGE } from '../../../core/config/color';
import {
  Avatar,
  Layout,
} from '@ui-kitten/components';
import firebase from 'react-native-firebase';
import { AccessToken, LoginManager } from 'react-native-fbsdk';
import { GoogleSignin } from '@react-native-community/google-signin';
import { StackActions, NavigationActions } from 'react-navigation';
import { MaterialIndicator } from 'react-native-indicators';
import { connect } from 'react-redux';
import _ from 'lodash'

class Drawer extends React.Component<{ navigation: any }, {}> {

  state = {
    showProcessView: false
  }

  render() {
    const { showProcessView } = this.state
    return (
      <View style={styles.container}>
        {this.userInfo()}
        {this.navItemList()}
        {showProcessView && <View style={styles.processView}>
          <View style={styles.processContainer}>
            <View style={{ height: 40, marginBottom: 15 }}>
              <MaterialIndicator color='white' />
            </View>
            <Text style={styles.processText}>Đang đăng xuất</Text>
          </View>
        </View>}
      </View>
    );
  }

  userInfo() {
    const { user } = this.props

    if (_.isEmpty(user)) {
      return null
    }

    return (
      <View style={styles.userInfoContainer}>
        <View style={styles.userInfoBody}>
          <View style={styles.avatarContainer}>
            <Avatar
              style={styles.avatar}
              shape='round'
              source={{ uri: user.image }}
              size={'giant'} />
          </View>
          <View >
            <Text style={styles.nameText}>{user.name}</Text>
          </View>
        </View>
      </View>
    );
  }

  navItemList() {
    const { items, activeItemKey } = this.props;

    return (
      <View>
        <FlatList
          data={items}
          key={activeItemKey}
          renderItem={this.navItem}
          ItemSeparatorComponent={this.itemSeparator}
        />
        {this.itemSeparator()}
        {this.commonNavItem('History', 'history', 'Lịch sử làm bài')}
        {this.itemSeparator()}
        {this.commonNavItem('Profile', 'user', 'Trang cá nhân')}
        {this.itemSeparator()}
        {this.logout()}
        {this.itemSeparator()}
      </View>
    );
  }

  itemSeparator() {
    return <View style={styles.itemSeparator} />;
  }

  logout = () => {
    return (
      <TouchableHighlight
        style={styles.navItem}
        underlayColor={'#E6E6E6'}
        onPress={this.logoutAction}
      >
        <View style={styles.navItemFlex}>
          <FontAwesome5Pro
            name={'sign-out'}
            size={20}
            solid
            color={RED_BOLD}
            style={styles.icon}
          />
          <Text style={styles.navItemTitle}>{'Đăng xuất'}</Text>
        </View>
      </TouchableHighlight>
    );
  };

  logoutAction = () => {
    this.setState({ showProcessView: true }, () => {
      firebase.auth().signOut().then(async () => {
        const isFacebookLogin = AccessToken.getCurrentAccessToken().then(res => res !== null)
        if (isFacebookLogin) {
          LoginManager.logOut()
          this.resetScreen()
          return
        }
        const isGmailLogin = await GoogleSignin.isSignedIn()
        if (isGmailLogin) {
          GoogleSignin.signOut().then(this.resetScreen)
        }
      })
    })
  }

  resetScreen = () => {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'Splash' })],
    });
    setTimeout(() => {
      this.props.navigation.dispatch(resetAction);
    }, 1000);
  }

  commonNavItem = (name, icon, title) => {
    return (
      <TouchableHighlight
        style={styles.navItem}
        underlayColor={'#E6E6E6'}
        onPress={() => {
          this.props.navigation.closeDrawer()
          this.switchNavigation(name)
        }}
      >
        <View style={styles.navItemFlex}>
          <FontAwesome5Pro
            name={icon}
            size={18}
            solid
            color={RED_BOLD}
            style={styles.icon}
          />
          <Text style={styles.navItemTitle}>{title}</Text>
        </View>
      </TouchableHighlight>
    )
  }

  navItem = ({ item }: any) => {
    const { activeItemKey } = this.props;
    const { routeName, key } = item;
    return (
      <TouchableHighlight
        style={styles.navItem}
        underlayColor={'#E6E6E6'}
        onPress={() => {
          this.props.navigation.closeDrawer()
          if (routeName !== 'Home') {
            this.props.navigation.navigate(routeName)
          }
        }}
      >
        <View style={styles.navItemFlex}>
          <FontAwesome5Pro
            name={this.nameIconMap(routeName)}
            size={18}
            solid
            color={RED_BOLD}
            style={styles.icon}
          />
          <Text style={styles.navItemTitle}>{this.nameMap(routeName)}</Text>
        </View>
      </TouchableHighlight>
    );
  };

  switchNavigation(routerName) {
    const { navigate } = this.props.navigation;
    navigate(routerName);
  }

  nameMap = (name: string): string | undefined => {
    switch (name) {
      case 'Home':
        return 'Trang chủ';
    }
  };

  nameIconMap = (name: string): string | undefined => {
    switch (name) {
      case 'Home':
        return 'home';
      case 'History':
        return 'history';
      case 'Profile':
        return 'user';
    }
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: WHITE
  },
  userInfoContainer: {
    flex: 0.2,
    backgroundColor: RED_BOLD,
  },
  avatarContainer: {
    width: 60,
    height: 60,
    marginRight: 30
  },
  userInfoBody: {
    flex: 1,
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  avatar: {
    // margin:10
  },
  logo: {
    width: 200,
    height: 60
  },
  nameText: {
    fontSize: 14,
    fontFamily: 'OpenSans-Bold',
    color: WHITE,
    marginBottom: 3
  },
  coinText: {
    fontSize: 14,
    fontFamily: 'OpenSans-Bold',
    color: ORANGE
  },
  title: {
    paddingTop: 10,
    color: 'white',
    fontSize: 13,
  },
  navItem: {
    height: 60,
  },
  navItemFlex: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1
  },
  navItemTitle: {
    fontSize: 14,
    color: RED_BOLD,
    fontFamily: 'OpenSans-Bold',
    marginLeft: 20
  },
  itemSeparator: {
    height: 1,
  },
  icon: {
    marginLeft: 20,
    width: 35
  },
  processView: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
  },
  processText: {
    fontSize: 13,
    fontFamily: 'OpenSans-Bold',
    color: WHITE,
  },
  processContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center'
  }
});

const mapStateToProps = state => {
  return {
    user: state.System.user
  }
}

export default connect(mapStateToProps)(Drawer)