import { GoogleSignin, statusCodes } from '@react-native-community/google-signin';
import _ from 'lodash';
import React, { Component } from 'react';
import { Image, LayoutAnimation, StatusBar, StyleSheet, Text, View } from 'react-native';
import { AccessToken, LoginManager } from "react-native-fbsdk";
import firebase from 'react-native-firebase';
import { authorizedHttp } from '../../../core/services/api';
import Button from '../../components/Button';
import { connect } from 'react-redux';
import { UserSetAction } from '../../../core/redux/action';
import { WHITE } from '../../../core/config/color';

const logo = require('../../../assets/images/sources/logo.png')
const fbButtonLabel = 'Đăng nhập với Facebook'
const fbButtonIcon = 'facebook-f'
const gmailButtonIcon = 'google-plus-g'
const gmailButtonLabel = 'Đăng nhập với Google'

class SplashScreen extends Component {

  state = {
    showLogin: false,
    disableLoginBtn: false
  }

  componentDidMount = () => {
    StatusBar.setHidden(true)
    this.checkForUserExist()
  }

  checkForUserExist = async () => {
    const currentUser = firebase.auth().currentUser

    if (currentUser !== null) {
      this.fetchUserInfo(currentUser.uid)
    } else {
      setTimeout(() => {
        this.showLoginAction()
      }, 1500);
    }
  }

  fetchUserInfo = (uid: string) => {
    try {
      this.setState({ disableLoginBtn: true }, async () => {
        const user = await authorizedHttp().get(`/user/${uid}`)
        this.props.setUserInfo(user.data)
        this.goToMainScreen()
      })
    } catch (e) {
      this.setState({ disableLoginBtn: false })
    }
  }

  showLoginAction = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.spring)
    this.setState({ showLogin: true })
  }

  fbButton = (disableLoginBtn) => (
    <Button
      rootStyle={{ ...styles.buttonContainer, backgroundColor: '#364e92' }}
      text={fbButtonLabel}
      iconColor={'white'}
      iconName={fbButtonIcon} iconSize={18}
      textStyle={styles.text}
      disabled={disableLoginBtn}
      onPress={this.loginFB} />
  )

  gmailButton = (disableLoginBtn) => (
    <Button
      rootStyle={{ ...styles.buttonContainer, backgroundColor: '#DD4B39' }}
      text={gmailButtonLabel}
      iconColor={'white'}
      iconName={gmailButtonIcon}
      iconSize={18}
      textStyle={styles.text}
      disabled={disableLoginBtn}
      onPress={this.signIn} />
  )

  loginFB = async () => {
    this.setState({ disableLoginBtn: true }, async () => {
      try {
        const token = await LoginManager
          .logInWithPermissions(['public_profile', 'email'])
          .then((result) => {
            if (result.isCancelled) {
              return Promise.reject('');
            }
            return AccessToken.getCurrentAccessToken();
          })
        const credential = firebase.auth.FacebookAuthProvider.credential(token.accessToken);
        const res = await firebase.auth().signInWithCredential(credential);

        this.handleCheckingUser(res)
      } catch (e) {
        this.setState({ disableLoginBtn: false })
      }
    })
  }


  signIn = () => {
    this.setState({ disableLoginBtn: true }, async () => {
      try {
        await GoogleSignin.signOut()
        await GoogleSignin.hasPlayServices();
        const userInfo = await GoogleSignin.signIn();
        const credential = firebase.auth.GoogleAuthProvider.credential(userInfo.idToken)
        const res = await firebase.auth().signInWithCredential(credential)

        this.handleCheckingUser(res)
      } catch (error) {
        this.setState({ disableLoginBtn: false })
        if (error.code === statusCodes.SIGN_IN_CANCELLED) {
          console.log('cancel')
          // user cancelled the login flow
        } else if (error.code === statusCodes.IN_PROGRESS) {
          // operation (e.g. sign in) is in progress already
        } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
          console.log('ko co google play')
          // play services not available or outdated
        } else {
          // some other error happened
        }
      }
    })
  };

  optionLine = () => (
    <View style={styles.optionContainer}>
      <View style={styles.pathLineView}>
        <View style={styles.line} />
      </View>
      <View style={styles.pathTextView}>
        <Text style={styles.optionText}>Hoặc</Text>
      </View>
      <View style={styles.pathLineView}>
        <View style={styles.line} />
      </View>
    </View>
  )

  handleCheckingUser = async (res) => {
    const userExist = await authorizedHttp().post(`/checkUserExists`, { id: res.user.uid })

    if (_.isEmpty(userExist.data)) {
      const userData = {
        image: res.user.photoURL,
        name: res.user.displayName,
        email: res.user.providerData[0].email,
        id: res.user.uid
      }

      const newUser = await authorizedHttp().post('/user', userData)
      this.props.setUserInfo(newUser.data)
      this.goToMainScreen()
    } else {
      this.props.setUserInfo(userExist.data)
      this.goToMainScreen()
    }
  }

  goToMainScreen = () => {
    this.props.navigation.navigate('MainApp')
    StatusBar.setHidden(false)
  }

  render() {
    const { showLogin, disableLoginBtn } = this.state

    return (
      <View style={styles.root}>
        <Image style={styles.logo} resizeMode={'contain'} source={logo} />
        {
          showLogin && <View style={styles.loginView}>
            {this.fbButton(disableLoginBtn)}
            {this.optionLine()}
            {this.gmailButton(disableLoginBtn)}
          </View>
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  optionContainer: {
    justifyContent: 'center',
    height: 20,
    flexDirection: 'row'
  },
  logo: {
    width: 200,
    height: 216
  },
  loginView: {
    marginLeft: 15,
    marginRight: 15
  },
  buttonContainer: {
    width: 300,
    height: 45,
    borderRadius: 5
  },
  optionText: {
    fontFamily: 'OpenSans-Extra',
    fontSize: 15,
  },
  text: {
    fontSize: 15,
    fontFamily: 'OpenSans-Bold',
    color: 'white',
    marginLeft: 15
  },
  pathLineView: {
    flex: 0.4,
    justifyContent: 'center',
    alignItems: 'center'
  },
  pathTextView: {
    flex: 0.2,
    justifyContent: 'center',
    alignItems: 'center'
  },
  line: {
    height: 0.5,
    backgroundColor: '#6E6E6E',
    width: '100%'
  },
})


function stateToProps(state) {
  return {
    user: state.user,
  };
}

function propsToState(dispatch) {
  return {
    setUserInfo: payload => dispatch(new UserSetAction(payload)),
  };
}

export default connect(stateToProps, propsToState)(SplashScreen)
