import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, Platform, TextInput, Dimensions, SafeAreaView, KeyboardAvoidingView, ScrollView } from 'react-native'
import { RED_BOLD, WHITE } from '../../../core/config/color'
import { TouchableWithoutFeedback, TouchableOpacity } from 'react-native-gesture-handler'
import { connect } from 'react-redux'
import Card from '../../components/Card'
import Header from '../../components/Header'
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5Pro'
import RoundButton from '../Home/components/RoundButton'
import ImagePicker from 'react-native-image-picker';
import firebase from 'react-native-firebase'
import RNFetchBlob from 'rn-fetch-blob'
import imageResizer from '../../../core/libs/imageResizer'
import { authorizedHttp } from '../../../core/services/api'
import { MaterialIndicator } from 'react-native-indicators'
import { UserSetAction } from '../../../core/redux/action'

class UserProfile extends Component {

  constructor(props) {
    super(props)

    this.state = {
      name: props.user.name,
      image: props.user.image,
      imagePicked: null,
      showProcessView: false
    }
  }

  middleViewHeader = () => {
    return (
      <View style={styles.headerCenterView}>
        <Text style={styles.headerText}>Profile</Text>
      </View>
    )
  }

  handlePickerImage = () => {
    ImagePicker.showImagePicker({
      title: 'Chọn ảnh',
      storageOptions: {
        skipBackup: true,
      },
      takePhotoButtonTitle: 'Chụp ảnh...',
      chooseFromLibraryButtonTitle: 'Chọn từ thư viện...',
      cancelButtonTitle: 'Hủy',
    }, response => {
      if (response.didCancel) {
        console.log(response);
      } else if (response.error) {
        console.log(response);
      } else if (response.customButton) {
      } else {
        imageResizer(response.uri, response.width, response.height, (newImage) => {
          this.setState({ imagePicked: newImage })
        }, (err) => {
          console.log(err);
        })
      }
    });
  }

  handleUpdateProfile = () => {
    const { email, id } = this.props.user
    const { name, image, imagePicked } = this.state
    let submitData = {}

    this.setState({ showProcessView: true }, async () => {
      if (imagePicked) {
        const imageRef = firebase.storage().ref(`images/${id}`).child(`${new Date().getTime()}`)

        const url = await imageRef.putFile(imagePicked.path, { contentType: 'image/jpeg' }).then(() => {
          return imageRef.getDownloadURL();
        }).then(url => url)

        submitData = {
          name,
          image: url,
          email: email
        }
      } else {
        submitData = {
          name,
          image: image,
          email: email
        }
      }

      authorizedHttp().put(`/user/${id}`, submitData).then(res => {
        this.setState({ showProcessView: false }, () => {
          this.props.setUserInfo(res.data)
        })
      }).catch(e => {
        this.setState({ showProcessView: false }, () => alert('Đã xảy ra lỗi'))
      })
    })
  }

  render() {
    const { email } = this.props.user
    const { name, image, imagePicked, showProcessView } = this.state

    return (
      <ScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps='never' style={styles.root} contentContainerStyle={styles.root}>
        <Header
          middleView={this.middleViewHeader}
          back
          navigation={this.props.navigation}
        />
        <View style={{ flex: 1 }}>
          <View style={styles.avatarView}>
            <TouchableWithoutFeedback onPress={this.handlePickerImage}>
              <Image source={{ uri: imagePicked ? imagePicked.uri : image }} style={styles.avatar} />
              <View style={styles.icon}>
                <FontAwesome5Icon name={'camera-alt'} color={WHITE} size={40} />
              </View>
            </TouchableWithoutFeedback>
          </View>
          <View style={styles.infoView}>
            <View style={styles.oneline}>
              <Text numberOfLines={1} style={styles.labelText}>Họ và tên</Text>
              <TextInput
                style={styles.contentText}
                onChangeText={text => this.setState({ name: text })}
                value={name}
                numberOfLines={1}
                placeholder={'Nhập ở đây...'}
              />
            </View>
            <View style={styles.oneline}>
              <Text numberOfLines={1} style={styles.labelText}>Email</Text>
              <Text numberOfLines={1} style={styles.contentText}>{email}</Text>
            </View>
          </View>
          <View style={styles.updateContainer}>
            <TouchableOpacity activeOpacity={0.6} style={[styles.commonView, { backgroundColor: 'white' }]} onPress={this.handleUpdateProfile}>
              <View style={styles.commonViewBody}>
                <Text style={styles.commonViewText}>{'Cập nhật'}</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        {showProcessView && <View style={styles.processView}>
          <View style={styles.processContainer}>
            <View style={{ height: 40, marginBottom: 15 }}>
              <MaterialIndicator color='white' />
            </View>
            <Text style={styles.processText}>Đang lưu</Text>
          </View>
        </View>}
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: RED_BOLD
  },
  avatarView: {
    flex: 0.4,
    justifyContent: 'center',
    alignItems: 'center'
  },
  avatar: {
    width: 160,
    height: 160,
    borderRadius: Platform.OS === 'android' ? 80 : 100
  },
  infoView: {
    height: 100,
    borderRadius: 4,
    backgroundColor: 'white',
    marginLeft: 15,
    marginRight: 15
  },
  labelText: {
    fontSize: 14,
    color: '#424242',
    fontFamily: 'OpenSans-Bold'
  },
  contentText: {
    flex: 0.8,
    fontSize: 14,
    color: RED_BOLD,
    fontFamily: 'OpenSans-Regular',
    textAlign: 'right'
  },
  headerText: {
    fontSize: 18,
    fontFamily: 'OpenSans-Bold',
    color: WHITE,
    marginRight: 45
  },
  headerCenterView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  oneline: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginLeft: 15,
    marginRight: 15
  },
  updateContainer: {
    flex: 0.15,
    justifyContent: 'center',
    alignItems: 'center'
  },
  icon: {
    opacity: 0.4,
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  processView: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    zIndex: 100
  },
  processText: {
    fontSize: 13,
    fontFamily: 'OpenSans-Bold',
    color: WHITE,
  },
  processContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center'
  },
  commonView: {
    height: 35,
    borderRadius: 20,
    backgroundColor: 'white',
    shadowColor: '#FFF',
    elevation: 1,
    shadowOffset: {
      width: 0,
      height: 2
    },
    margin: 5
  },
  commonViewText: {
    fontSize: 14,
    fontFamily: 'OpenSans-Bold',
    color: RED_BOLD,
    margin: 10
  },
  commonViewBody: {
    flex: 1,
    marginHorizontal: 10,
    marginVertical: 12,
    justifyContent: 'center',
    alignItems: 'center',
  },
})

const mapStateToProps = state => {
  return {
    user: state.System.user
  }
}


function propsToState(dispatch) {
  return {
    setUserInfo: payload => dispatch(new UserSetAction(payload)),
  };
}

export default connect(mapStateToProps, propsToState)(UserProfile)