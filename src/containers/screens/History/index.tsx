import React, { Component } from 'react'
import { ScrollView, StyleSheet, Text, View } from 'react-native'
import { BACKGROUND_COMMON, RED_BOLD, WHITE } from '../../../core/config/color'
import Header from '../../components/Header'
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5Pro'
import { Avatar } from '@ui-kitten/components'
import { Subject, getSubjectImage } from '../../../core/libs/constant/subject'
import { connect } from 'react-redux'
import List from '../../components/List'
import moment from 'moment'

class History extends Component {
  list = React.createRef<List>()

  historyItem = (date, startTime, finishTime, year, name, correctCount, totalQues, totalPoint, subject_id) => {
    const subjectName = Subject[subject_id]
    const subjectImage = getSubjectImage(subject_id)
    return (
      <View style={styles.historyRoot}>
        <View style={styles.pointView}>
          <View style={{ height: 25, width: 1, backgroundColor: RED_BOLD }} />
          <FontAwesome5Icon regular name={'clock'} size={20} color={RED_BOLD} />
          <View style={{ height: 20, width: 1, backgroundColor: RED_BOLD }} />
          <View style={{ width: 6, height: 6, borderRadius: 3, backgroundColor: RED_BOLD }} />
          <Text style={styles.pointText}>{totalPoint}</Text>
          <View style={{ width: 6, height: 6, borderRadius: 3, backgroundColor: RED_BOLD }} />
          <View style={{ height: 60, width: 1, backgroundColor: RED_BOLD }} />
        </View>
        <View style={styles.descriptionView}>
          <View style={styles.descriptionContent}>
            <View style={styles.timeView}>
              <Text style={styles.dateText}>{date}</Text>
              <View style={styles.subjectAndImage}>
                <Text style={styles.dateText}>{subjectName}</Text>
                <Avatar size={'tiny'} source={subjectImage} style={{ marginLeft: 8, marginRight: 5, marginBottom: 8 }} />
              </View>
            </View>
            <Text style={styles.titleText}>{startTime} - {name} năm {year}</Text>
            <Text style={styles.normalText}>Bạn đã hoàn thành bài thi này trong thời gian {finishTime}</Text>
            <Text style={styles.normalText}>Bạn đạt được {correctCount}/{totalQues} đáp án đúng</Text>
            <View style={{ flex: 1, justifyContent: 'flex-end' }}>
              <View style={{ height: 1, backgroundColor: '#E6E6E6' }}></View>
            </View>
          </View>
        </View>
      </View>
    )
  }

  middleViewHeader = () => {
    return (
      <View style={styles.headerCenterView}>
        <Text style={styles.headerText}>Lịch sử làm bài</Text>
      </View>
    )
  }

  render() {
    const { user } = this.props

    return (
      <View style={styles.root}>
        <Header
          middleView={this.middleViewHeader}
          back
          navigation={this.props.navigation}
        />
        <View style={{flex: 1}}>
          <List
            url={`/history?_order=DESC&_sort=id&user_id=${user.id}`}
            renderItem={this.listItem}
          />
        </View>
      </View>
    )
  }

  listItem = ({ item, index }) => {
    const { navigate } = this.props.navigation
    const startDate = moment(item.start_time).format('DD/MM/YYYY')
    const startTime = moment(item.start_time).format('HH:mm')
    const finishTime = moment.utc(item.finish_time * 1000).format('HH:mm:ss')

    return this.historyItem(
      startDate,
      startTime,
      finishTime,
      item.history_exam.year,
      item.history_exam.name,
      Math.ceil(item.point * item.history_exam.number_of_question / 10),
      item.history_exam.number_of_question,
      item.point.toFixed(2),
      item.history_exam.subject_id
    )
  };
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: BACKGROUND_COMMON
  },
  timeView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  headerText: {
    fontSize: 18,
    fontFamily: 'OpenSans-Bold',
    color: WHITE,
    marginRight: 45
  },
  headerCenterView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  historyRoot: {
    height: 210,
    flexDirection: 'row',
  },
  pointView: {
    flex: 0.27,
    alignItems: 'center'
  },
  descriptionView: {
    flex: 0.73,
    alignItems: 'flex-start'
  },
  pointText: {
    fontSize: 30,
    fontFamily: 'OpenSans-Bold',
    color: RED_BOLD,
    marginVertical: 10
  },
  descriptionContent: {
    flex: 1,
    marginTop: 20,
    marginRight: 15,
    justifyContent: 'flex-start',
  },
  dateText: {
    color: RED_BOLD,
    fontSize: 16,
    fontFamily: 'OpenSans-Bold',
    marginBottom: 10
  },
  titleText: {
    color: '#424242',
    fontSize: 16,
    fontFamily: 'OpenSans-Bold',
    marginBottom: 10
  },
  normalText: {
    color: '#BDBDBD',
    fontSize: 15,
    fontFamily: 'OpenSans-Regular'
  },
  subjectAndImage: {
    flexDirection: 'row',
    alignItems: 'center'
  },
})

const mapStateToProps = state => {
  return {
    user: state.System.user
  }
}

export default connect(mapStateToProps)(History)