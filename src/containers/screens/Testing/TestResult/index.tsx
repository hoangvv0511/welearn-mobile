import React, { Component } from 'react'
import { Dimensions, ScrollView, StyleSheet, Text, View, Alert, BackHandler } from 'react-native'
import { PieChart } from "react-native-chart-kit"
import { connect } from 'react-redux'
import { RED_BOLD, WHITE } from '../../../../core/config/color'
import { getResultMessage } from '../../../../core/libs/constant/point'
import { getThematicEnum } from '../../../../core/libs/constant/subject'
import { authorizedHttp } from '../../../../core/services/api'
import RoundButton from '../../Home/components/RoundButton'
import RatingModal from '../components/RatingModal'

const chartConfig = {
  backgroundGradientFrom: "#1E2923",
  backgroundGradientFromOpacity: 0,
  backgroundGradientTo: "#08130D",
  backgroundGradientToOpacity: 0.5,
  color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})`,
  strokeWidth: 2, // optional, default 3
  barPercentage: 0.5
};

class TestResult extends Component {
  ratingModal = React.createRef<RatingModal>()

  constructor(props) {
    super(props)

    this.state = {
      data: [
        {
          name: "câu đúng",
          population: 0,
          color: "green",
          legendFontColor: RED_BOLD,
          legendFontSize: 15
        },
        {
          name: "câu chưa làm",
          population: 0,
          color: "yellow",
          legendFontColor: RED_BOLD,
          legendFontSize: 15,
        },
        {
          name: "câu sai",
          population: 0,
          color: "red",
          legendFontColor: RED_BOLD,
          legendFontSize: 15
        },
      ],
      arrayThematicNeedtoPractice: [],
      thematicEnum: getThematicEnum(this.props.navigation.state.params.examData.subject_id),
      isRated: this.props.navigation.state.params.examData.is_rated
    }
  }

  componentDidMount = () => {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackNativePress)
    const { examData, answers, answerByThematic, examResult } = this.props.navigation.state.params
    let { data, arrayThematicNeedtoPractice, isRated } = this.state

    if (!isRated) {
      setTimeout(() => {
        this.ratingModal.current.open()
      }, 500);
    }

    data[0].population = examResult.correct
    data[1].population = examResult.notChoice
    data[2].population = examResult.wrong

    answerByThematic.forEach(e => {
      if (e.needPractice) {
        arrayThematicNeedtoPractice.push(e.slug)
      }
    })

    this.setState({
      data,
      arrayThematicNeedtoPractice
    }, () => {
      setTimeout(() => {
        this.forceUpdate()
      }, 100);
    })
  }

  handleRatePress = () => {
    const { id } = this.props.user
    const { examData } = this.props.navigation.state.params
    const stars = this.ratingModal.current.getCurrentStar()

    const submitData = {
      user_id: id,
      exam_id: examData.id,
      star: stars
    }

    authorizedHttp().post('/addStar', submitData).then(res => this.setState({ isRated: true })).catch(e => console.log('e', e))
    this.ratingModal.current.close()
  }

  handleTestAgainPress = () => {
    Alert.alert(
      'Thông báo',
      `Bạn có chắc chắn muốn làm lại đề thi này không ?`,
      [
        {
          text: 'Hủy bỏ', style: 'cancel',
        },
        {
          text: 'Đồng ý', onPress: () => this.props.navigation.replace('Testing', { examData: { ...this.props.navigation.state.params.examData, is_rated: this.state.isRated } })
        }
      ],
      { cancelable: true }
    )
  }

  handleBackNativePress = () => {
    const { pop, state: { params: { onBack } } } = this.props.navigation
    pop()
    onBack && onBack()
    return true
  }

  componentWillUnmount = () => {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackNativePress)
  }

  handleBackPress = () => {
    const { pop, state: { params: { onBack } } } = this.props.navigation
    pop()
    onBack && onBack()
  }

  handleAnswerPress = () => {
    const { examData, answers, examResult } = this.props.navigation.state.params

    this.props.navigation.navigate('TestResultFullAnswer', {
      answers,
      examData,
      examResult
    })
  }

  handleCommentPress = () => {
    const { examData, answers, examResult, answerByThematic } = this.props.navigation.state.params

    this.props.navigation.navigate('TestResultDescription', {
      answers,
      examData,
      examResult,
      answerByThematic
    })
  }

  render() {
    const { examData, examResult, disabledTestAgain } = this.props.navigation.state.params
    const { data, arrayThematicNeedtoPractice, thematicEnum } = this.state

    return (
      <ScrollView style={styles.root} showsVerticalScrollIndicator={false}>
        <View style={styles.messageView}>
          <Text style={styles.messageText}>{getResultMessage(examResult.totalPoint)}</Text>
        </View>
        <View>
          <Text numberOfLines={1} style={styles.pointText}>{examResult.totalPoint} points</Text>
        </View>
        <View>
          <Text numberOfLines={1} style={styles.numberAnswerText}>{`${examResult.correct}/${examData.number_of_question}`}</Text>
        </View>
        {data.length && <View style={{ backgroundColor: WHITE, marginVertical: 20 }}>
          <PieChart
            data={data}
            width={Dimensions.get('window').width}
            height={220}
            chartConfig={chartConfig}
            accessor="population"
            backgroundColor="transparent"
            paddingLeft="10"
            absolute
          />
        </View>}
        {arrayThematicNeedtoPractice.length ? <View>
          <Text style={styles.suggestTitleText}>Bạn nên rèn luyện thêm các chuyên đề sau :</Text>
          {
            arrayThematicNeedtoPractice.map(thematic => {
              return (
                <Text style={styles.suggestText}>{`+ ${thematicEnum[thematic]}`}</Text>
              )
            })
          }
        </View> : null}
        <View style={{ marginVertical: 20 }}>
          {!disabledTestAgain && <RoundButton text={'Làm lại bài thi'} backgroundColor={WHITE} rootStyle={{ marginHorizontal: 40 }} onPress={this.handleTestAgainPress} />}
          <RoundButton text={'Xem nhận xét'} backgroundColor={WHITE} rootStyle={{ marginHorizontal: 40 }} onPress={this.handleCommentPress} />
          <RoundButton text={'Xem đáp án'} backgroundColor={WHITE} rootStyle={{ marginHorizontal: 40 }} onPress={this.handleAnswerPress} />
          <RoundButton text={'Quay lại'} backgroundColor={WHITE} rootStyle={{ marginHorizontal: 40 }} onPress={this.handleBackPress} />
        </View>
        <RatingModal
          ref={this.ratingModal}
          name={examData.name}
          year={examData.year}
          onRatePress={this.handleRatePress}
        />
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: RED_BOLD
  },
  messageText: {
    fontSize: 16,
    fontFamily: 'OpenSans-Regular',
    color: WHITE,
    textAlign: 'center'
  },
  messageView: {
    marginLeft: 40,
    marginRight: 40,
    marginVertical: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  pointText: {
    fontSize: 44,
    fontFamily: 'OpenSans-Bold',
    color: WHITE,
    textAlign: 'center',
    marginBottom: 10
  },
  numberAnswerText: {
    fontSize: 27,
    fontFamily: 'OpenSans-Regular',
    color: WHITE,
    textAlign: 'center'
  },
  suggestTitleText: {
    fontSize: 15,
    fontFamily: 'OpenSans-BoldItalic',
    color: WHITE,
    marginLeft: 20,
    marginBottom: 5
  },
  suggestText: {
    fontSize: 14,
    fontFamily: 'OpenSans-Regular',
    color: WHITE,
    marginLeft: 20,
    marginVertical: 5
  }
})

const mapStateToProps = state => {
  return {
    user: state.System.user
  }
}

export default connect(mapStateToProps)(TestResult)