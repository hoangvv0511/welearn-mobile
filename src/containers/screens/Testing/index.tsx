import moment from 'moment';
import React, { Component } from 'react';
import { Alert, BackHandler, LayoutAnimation, SafeAreaView, StatusBar, StyleSheet, Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { MaterialIndicator } from 'react-native-indicators';
import Orientation from 'react-native-orientation-locker';
import Pdf from 'react-native-pdf';
import Icon from 'react-native-vector-icons/FontAwesome5Pro';
import { RED_BOLD, WHITE } from '../../../core/config/color';
import AnswerList from '../../components/AnswerList';
import IconWithText from '../../components/IconWithText';
import Timer from '../../components/Timer';
import { Answer } from '../../components/AnswerList/AnswerItem';
import { getThematicEnum } from '../../../core/libs/constant/subject';
import { authorizedHttp } from '../../../core/services/api';
import { connect } from 'react-redux';

const ANSWER_VIEW_MIN_WIDTH = 10
const ANSWER_VIEW_MAX_WIDTH = 150
const SWITCH_ICON_LEFT = 'chevron-circle-left'
const SWITCH_ICON_RIGHT = 'chevron-circle-right'
const regex = /\id=(.*)/

class Testing extends Component {
  timer = React.createRef<Timer>()

  state = {
    answerWidth: ANSWER_VIEW_MAX_WIDTH,
    switchIcon: SWITCH_ICON_RIGHT,
    loading: true,
    answerData: [],
    showProcessView: false,
    startTime: null,
    endTime: null,
    finishedTime: null,
    maxPages: -1,
    currentPage: -1,
    disableAnswer: false
  }

  componentDidMount = () => {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
    Orientation.lockToLandscape()
  }

  handleBackPress = () => {
    this.outRoomConfimationModal()
    return true
  }

  componentWillUnmount = () => {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
  }

  outRoomConfimationModal = () => {
    const { pop } = this.props.navigation

    Alert.alert(
      'Thông báo',
      'Nếu bạn thoát phòng hệ thống sẽ không ghi nhận kết quả làm bài, vẫn tiếp tục ?',
      [
        {
          text: 'Hủy bỏ', style: 'cancel',
        },
        {
          text: 'Đồng ý', onPress: () => {
            Orientation.lockToPortrait()
            pop()
          }
        }
      ],
      { cancelable: true }
    )
    return
  }

  notDoneConfirmationModal = (finishedTime) => {
    let { answerData } = this.state
    const { examData } = this.props.navigation.state.params

    Alert.alert(
      'Thông báo',
      `Bạn vẫn còn ${examData.number_of_question - answerData.length} câu chưa làm.`,
      [
        {
          text: 'Hủy bỏ', style: 'cancel',
        },
        {
          text: 'Đồng ý', onPress: () => this.timeLeftConfirmationModal(finishedTime)
        }
      ],
      { cancelable: true }
    )
  }

  timeLeftConfirmationModal = (finishedTime) => {
    this.setState({ finishedTime }, () => {
      Alert.alert(
        'Xác nhận',
        `Bạn có muốn kết thúc bài thi trong thời gian [${moment.utc(finishedTime * 1000).format('HH:mm:ss')}]`,
        [
          {
            text: 'Hủy bỏ', style: 'cancel',
          },
          {
            text: 'Đồng ý', onPress: () => this.calculateResult(finishedTime)
          }
        ],
        { cancelable: true }
      )
    })
  }

  handleChangeAnswerLayout = () => {
    const { answerWidth, switchIcon } = this.state
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({
      answerWidth: answerWidth === ANSWER_VIEW_MAX_WIDTH ? ANSWER_VIEW_MIN_WIDTH : ANSWER_VIEW_MAX_WIDTH,
      switchIcon: switchIcon === SWITCH_ICON_RIGHT ? SWITCH_ICON_LEFT : SWITCH_ICON_RIGHT
    })
  }

  iconButton = (name: string, color: string, size: number, onPress: any) => {
    return (
      <TouchableOpacity activeOpacity={0.5} onPress={onPress}>
        <Icon solid name={name} color={color} size={size} />
      </TouchableOpacity>
    )
  }

  handleProcessExam = () => {
    const { answerData } = this.state
    const { examData } = this.props.navigation.state.params
    const time = this.timer.current.getCountLeft()
    const finishedTime = examData.exam_time * 60 - time

    if (answerData.length < examData.number_of_question && time > 0) {
      this.notDoneConfirmationModal(finishedTime)
    } else if (answerData.length >= examData.number_of_question && time > 0) {
      this.timeLeftConfirmationModal(finishedTime)
    } else {
      this.calculateResult(finishedTime)
    }
  }

  calculateResult = (finishedTime) => {
    const { answerData, startTime } = this.state
    const { user } = this.props
    const { examData: { array_answer, array_thematic, array_difficulty, subject_id }, examData } = this.props.navigation.state.params

    this.setState({ showProcessView: true }, () => {
      let thematicArr = Object.keys(getThematicEnum(subject_id)).map(x => { return { slug: x, answer: [] } })

      const newArr = array_answer.map((answer, index) => {
        const quesId = index + examData.start_id
        const userAnswerIndex = answerData.findIndex(x => x.id === quesId)
        const correctAnswer = Answer[answer]
        const thematic = array_thematic[index]
        const userAnswer = userAnswerIndex > -1 ? answerData[userAnswerIndex].answer : 0

        return {
          id: quesId,
          correctAnswer,
          thematic,
          difficulty: array_difficulty[index],
          userAnswer,
        }
      })

      let pointPerQuestion = 10 / examData.number_of_question
      let correctCount = 0
      let notDoneCount = 0
      let wrongCount = 0
      let totalPoint = 0

      newArr.forEach((element, index) => {
        const thematicIndex = thematicArr.findIndex(x => {
          return x.slug === element.thematic
        })
        thematicArr[thematicIndex].answer.push(element)

        if (element.userAnswer === 0) {
          notDoneCount++
        } else if (element.correctAnswer === element.userAnswer) {
          totalPoint += pointPerQuestion
          correctCount++
        } else {
          wrongCount++
        }
      });

      const answerByThematic = thematicArr.map(thematicItem => {
        let wrongAnswer = 0
        thematicItem.answer.forEach(result => {
          if (result.correctAnswer !== result.userAnswer) {
            wrongAnswer++
          }
        })

        return {
          ...thematicItem,
          needPractice: wrongAnswer > thematicItem.answer.length / 2
        }
      })

      const submitData = {
        point: totalPoint,
        start_time: startTime,
        finish_time: finishedTime,
        user_id: user.id,
        exam_id: examData.id,
      }

      authorizedHttp().post('/history', submitData)
      const { replace, state: { params: { onBack } } } = this.props.navigation
      setTimeout(() => {
        this.setState({ showProcessView: false }, () => {
          Orientation.lockToPortrait()
          replace('TestResult', {
            examData,
            answers: newArr,
            answerByThematic,
            examResult: {
              totalPoint: totalPoint.toFixed(2),
              correct: correctCount,
              wrong: wrongCount,
              notChoice: notDoneCount,
              finishedTime
            },
            onBack: onBack
          })
        })
      }, 500);
    })
  }

  onTimerChange = (timeLeft) => {
    this.setState({ disableAnswer: true })
  }

  header = () => {
    let { switchIcon, answerData } = this.state
    const { examData, endTime } = this.props.navigation.state.params

    return (
      <View style={styles.headerLandscape}>
        <View style={styles.backView}>
          {this.iconButton('arrow-circle-left', 'white', 25, this.outRoomConfimationModal)}
        </View>
        <View style={styles.timerView}>
          <Timer
            ref={this.timer}
            countFrom={endTime ? (endTime - new Date().getTime()) / 1000 : examData.exam_time * 60}
            mode={'control'}
            onFinish={this.onTimerChange}
          />
        </View>
        <TouchableOpacity activeOpacity={1} style={[styles.commonView, { marginLeft: 20 }]}>
          <View style={styles.commonViewBody}>
            <Text style={styles.commonViewText}>{`Đã làm: ${answerData.length}/${examData.number_of_question}`}</Text>
          </View>
        </TouchableOpacity>
        <View style={styles.switchButtonView}>
          <TouchableOpacity style={[styles.commonView, { marginRight: 20 }]} onPress={this.handleProcessExam}>
            <View style={styles.commonViewBody}>
              <IconWithText
                iconColor={RED_BOLD}
                iconName={'layer-plus'}
                iconSize={14}
                text={'Nộp bài'}
                textStyle={styles.commonViewText}
              />
            </View>
          </TouchableOpacity>
          {this.iconButton(switchIcon, 'white', 25, this.handleChangeAnswerLayout)}
        </View>
      </View>
    )
  }

  onPageChange = (page, numberOfPages) => this.setState({
    maxPages: numberOfPages,
    currentPage: page
  })

  onLoadComplete = (numberOfPages, filePath) => {
    const { examData } = this.props.navigation.state.params
    const startTime = new Date().getTime()
    this.setState({
      loading: false,
      startTime: startTime,
      endTime: startTime + examData.exam_time * 60 * 1000
    })
    this.timer.current.play()
  }

  pdfView = () => {
    const { examData } = this.props.navigation.state.params
    const { maxPages, currentPage } = this.state
    const id = examData.question.match(regex)[1]
    const source = { uri: `https://drive.google.com/u/1/uc?id=${id}&export=download`, cache: true };

    return (
      <View style={{ flex: 1 }}>
        <Pdf
          source={source}
          scale={2}
          minScale={2}
          onLoadComplete={this.onLoadComplete}
          onPageChanged={this.onPageChange}
          onError={(error) => console.log('error', error)}
          style={styles.pdf} />
        {maxPages > -1 && <View style={styles.paginateView}>
          <Text style={styles.pageText}>{`${currentPage}/${maxPages}`}</Text>
        </View>}
      </View>
    )
  }

  answerList = () => {
    const { examData } = this.props.navigation.state.params
    const { disableAnswer } = this.state

    return (
      <AnswerList
        disabled={disableAnswer}
        startId={examData.start_id}
        numberOfQuestion={examData.number_of_question}
        onAnswerChange={(res) => this.setState({ answerData: res })}
      />
    )
  }

  render() {
    const { answerWidth, showProcessView } = this.state

    return (
      <SafeAreaView style={styles.root}>
        <StatusBar hidden />
        <View style={styles.content}>
          <View style={styles.examView}>
            {this.header()}
            {this.pdfView()}
          </View>
          <View style={[styles.answerView, { width: answerWidth }]}>
            {this.answerList()}
          </View>
        </View>
        {showProcessView && <View style={styles.processView}>
          <View style={styles.processContainer}>
            <View style={{ height: 40, marginBottom: 15 }}>
              <MaterialIndicator color='white' />
            </View>
            <Text style={styles.processText}>Đang thống kê kết quả</Text>
          </View>
        </View>}
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1
  },
  content: {
    flex: 1,
    flexDirection: 'row'
  },
  examView: {
    flex: 1,
  },
  answerView: {
    right: 0,
    borderColor: RED_BOLD,
    borderWidth: 1
  },
  headerLandscape: {
    height: 40,
    backgroundColor: RED_BOLD,
    flexDirection: 'row',
    alignItems: 'center'
  },
  switchButtonView: {
    justifyContent: 'flex-end',
    flexDirection: 'row',
    flex: 1,
    marginRight: 10
  },
  backView: {
    marginLeft: 17
  },
  pdf: {
    flex: 1,
  },
  timerView: {
    marginLeft: 17
  },
  commonView: {
    borderRadius: 4,
    backgroundColor: 'white',
    shadowColor: '#FFF',
    elevation: 1,
    shadowOffset: {
      width: 0,
      height: 2
    }
  },
  commonViewText: {
    fontSize: 13,
    fontFamily: 'OpenSans-Bold',
    color: RED_BOLD
  },
  commonViewBody: {
    flex: 1,
    marginHorizontal: 10,
    marginVertical: 12,
    justifyContent: 'center',
    alignItems: 'center',
  },
  processView: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
  },
  processText: {
    fontSize: 13,
    fontFamily: 'OpenSans-Bold',
    color: WHITE,
  },
  processContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center'
  },
  pageText: {
    fontSize: 13,
    fontFamily: 'OpenSans-Bold',
    color: 'black',
    margin: 5
  },
  paginateView: {
    position: 'absolute',
    bottom: 10,
    right: 15,
    backgroundColor: '#e6e6e6'
  }
})

const mapStateToProps = state => {
  return {
    user: state.System.user
  }
}

export default connect(mapStateToProps)(Testing)