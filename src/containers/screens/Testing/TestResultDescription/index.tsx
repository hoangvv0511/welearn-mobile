import React, { Component } from 'react'
import { Text, View, StyleSheet, ScrollView } from 'react-native'
import { RED_BOLD, WHITE, BACKGROUND_COMMON } from '../../../../core/config/color'
import { getThematicEnum, Difficulty } from '../../../../core/libs/constant/subject'
import Header from '../../../components/Header'

class TestResultDescription extends Component {


  thematicHeader = (thematic: string, totalQues: number) => {
    return (
      <View style={styles.thematicRoot}>
        <View style={styles.thematicView}>
          <Text style={styles.thematicText}>{thematic}</Text>
        </View>
        <View style={styles.totalQuesView}>
          <Text style={styles.totalQuesText}>{`Tổng câu: ${totalQues}`}</Text>
        </View>
      </View>
    )
  }

  questionView = (id: number, level: string, isCorrect: boolean) => {
    return (
      <View style={styles.questionRoot}>
        <View style={{ width: 70 }}>
          <Text style={styles.quesText}>{`Câu ${id}`}</Text>
        </View>
        <View style={styles.levelView}>
          <Text>{level}</Text>
        </View>
        <View style={[styles.correctView, {
          backgroundColor: isCorrect ? '#81F781' : '#fec0cd',
          borderColor: isCorrect ? 'green' : 'red',
        }]}>
          <Text>{`${isCorrect ? 'Đúng' : 'Sai'}`}</Text>
        </View>
      </View>
    )
  }

  middleViewHeader = () => {
    return (
      <View style={styles.headerCenterView}>
        <Text style={styles.headerText}>Nhận xét</Text>
      </View>
    )
  }

  render() {
    const { answerByThematic, examData } = this.props.navigation.state.params

    return (
      <ScrollView style={styles.root}>
        <Header
          middleView={this.middleViewHeader}
          back
          navigation={this.props.navigation}
        />
        <View>
          {
            answerByThematic.map(item => {
              return (
                item.answer.length ? <View style={styles.content}>
                  {this.thematicHeader(getThematicEnum(examData.subject_id)[item.slug], item.answer.length)}
                  {
                    item.answer.map(answer => this.questionView(answer.id, Difficulty[answer.difficulty], answer.correctAnswer === answer.userAnswer))
                  }
                </View> : null
              )
            })
          }
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: BACKGROUND_COMMON
  },
  thematicText: {
    fontSize: 14,
    fontFamily: 'OpenSans-Bold',
    color: WHITE,
    margin: 5
  },
  content: {
    marginVertical: 15, 
    backgroundColor: WHITE
  },
  thematicRoot: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: RED_BOLD,
    borderWidth: 2,
  },
  thematicView: {
    flex: 0.75,
    backgroundColor: RED_BOLD,
    justifyContent: 'center'
  },
  totalQuesView: {
    flex: 0.25,
  },
  totalQuesText: {
    fontSize: 14,
    fontFamily: 'OpenSans-Bold',
    color: '#424242',
    marginLeft: 5
  },
  questionRoot: {
    flexDirection: 'row',
    alignItems: 'center',
    margin: 9,
  },
  levelView: {
    width: 130,
    height: 25,
    backgroundColor: '#aeeeee',
    borderColor: '#6ba1a1',
    borderWidth: 2,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 30,
    borderRadius: 4
  },
  correctView: {
    width: 60,
    height: 25,
    borderWidth: 2,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 40,
    borderRadius: 4
  },
  quesText: {
    fontSize: 14,
    fontFamily: 'OpenSans-Bold',
    marginLeft: 5
  },
  headerText: {
    fontSize: 18,
    fontFamily: 'OpenSans-Bold',
    color: WHITE,
    marginRight: 45
  },
  headerCenterView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
})

export default TestResultDescription