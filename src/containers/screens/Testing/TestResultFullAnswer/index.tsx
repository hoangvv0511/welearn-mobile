import React, { Component } from 'react';
import { BackHandler, LayoutAnimation, SafeAreaView, ScrollView, StatusBar, StyleSheet, Text, View, Dimensions, FlatList } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Orientation from 'react-native-orientation-locker';
import Pdf from 'react-native-pdf';
import Icon from 'react-native-vector-icons/FontAwesome5Pro';
import { RED_BOLD, WHITE } from '../../../../core/config/color';
import AnswerList from '../../../components/AnswerList';
import IconWithText from '../../../components/IconWithText';
import moment from 'moment';

const ANSWER_VIEW_MIN_WIDTH = 10
const ANSWER_VIEW_MAX_WIDTH = 150
const SWITCH_ICON_LEFT = 'chevron-circle-left'
const SWITCH_ICON_RIGHT = 'chevron-circle-right'
const regex = /\id=(.*)/

export default class TestResultFullAnswer extends Component {
  scrollView = React.createRef<ScrollView>()

  constructor(props) {
    super(props)

    this.state = {
      answerWidth: ANSWER_VIEW_MAX_WIDTH,
      switchIcon: SWITCH_ICON_RIGHT,
      loading: true,
      answerData: [],
      showProcessView: false,
      startTime: null,
      endTime: null,
      maxPages: -1,
      currentPage: -1,
      maxAnswerPages: -1,
      currentAnswerPage: -1,
      disableAnswer: false,
      showAnswerFile: false,
      width: Dimensions.get('window').width,
      height: Dimensions.get('window').height,
    }

    Dimensions.addEventListener("change", (e) => {
      this.setState(e.window);
    });
  }

  componentDidMount = () => {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
    Orientation.lockToLandscape()
  }

  handleBackPress = () => {
    Orientation.lockToPortrait()
  }

  handleBackButtonPress = () => {
    Orientation.lockToPortrait()
    this.props.navigation.pop()
  }

  componentWillUnmount = () => {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
  }

  handleChangeAnswerLayout = () => {
    const { answerWidth, switchIcon } = this.state
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({
      answerWidth: answerWidth === ANSWER_VIEW_MAX_WIDTH ? ANSWER_VIEW_MIN_WIDTH : ANSWER_VIEW_MAX_WIDTH,
      switchIcon: switchIcon === SWITCH_ICON_RIGHT ? SWITCH_ICON_LEFT : SWITCH_ICON_RIGHT
    })
  }

  iconButton = (name: string, color: string, size: number, onPress: any) => {
    return (
      <TouchableOpacity activeOpacity={0.5} onPress={onPress}>
        <Icon solid name={name} color={color} size={size} />
      </TouchableOpacity>
    )
  }

  handleProcessExam = () => {
    this.setState({ showAnswerFile: !this.state.showAnswerFile }, () => {
      this.scrollView.current.scrollTo({ x: !this.state.showAnswerFile ? 0 : this.state.width, animated: true })
    })
  }

  header = () => {
    let { switchIcon, answerData, showAnswerFile } = this.state
    const { examData, examResult } = this.props.navigation.state.params

    return (
      <View style={styles.headerLandscape}>
        <View style={styles.backView}>
          {this.iconButton('arrow-circle-left', 'white', 25, this.handleBackButtonPress)}
        </View>
        <TouchableOpacity activeOpacity={1} style={[styles.commonView, { marginLeft: 20 }]}>
          <View style={styles.commonViewBody}>
            <Text style={styles.commonViewText}>{`Điểm: ${examResult.totalPoint}`}</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity activeOpacity={1} style={[styles.commonView, { marginLeft: 10 }]}>
          <View style={styles.commonViewBody}>
            <Text style={styles.commonViewText}>{`Đúng: ${examResult.correct}/${examData.number_of_question}`}</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity activeOpacity={1} style={[styles.commonView, { marginLeft: 20 }]}>
          <View style={styles.commonViewBody}>
            <Text style={styles.commonViewText}>{`Thời gian: ${moment.utc(examResult.finishedTime * 1000).format('HH:mm:ss')}`}</Text>
          </View>
        </TouchableOpacity>
        <View style={styles.switchButtonView}>
          <TouchableOpacity activeOpacity={0.5} style={[styles.commonView, { marginRight: 20 }]} onPress={this.handleProcessExam}>
            <View style={styles.commonViewBody}>
              <IconWithText
                iconColor={RED_BOLD}
                iconName={'file-pdf'}
                iconSize={14}
                text={showAnswerFile ? 'Xem đáp án' : 'Xem đề'}
                textStyle={styles.commonViewText}
              />
            </View>
          </TouchableOpacity>
          {this.iconButton(switchIcon, 'white', 25, this.handleChangeAnswerLayout)}
        </View>
      </View>
    )
  }

  onPageExamChange = (page, numberOfPages) => this.setState({
    maxPages: numberOfPages,
    currentPage: page
  })

  onPageAnswerChange = (page, numberOfPages) => this.setState({
    maxAnswerPages: numberOfPages,
    currentAnswerPage: page
  })

  pdfView = () => {
    const { examData } = this.props.navigation.state.params
    const { maxPages, currentPage, maxAnswerPages, currentAnswerPage } = this.state
    const sourceExam = { uri: `https://drive.google.com/u/1/uc?id=${examData.question.match(regex)[1]}&export=download`, cache: true }
    const sourceAnswer = { uri: `https://drive.google.com/u/1/uc?id=${examData.answer.match(regex)[1]}&export=download`, cache: true }

    return (
      <View style={{ flex: 1 }}>
        <ScrollView
          ref={this.scrollView}
          pagingEnabled
          horizontal
          showsHorizontalScrollIndicator={false}
          scrollEnabled={false}
          style={{ flex: 1 }}
          contentContainerStyle={{ width: this.state.width * 2 }}
        >
          <View key={0} style={{ flex: 0.5 }}>
            <Pdf
              source={sourceAnswer}
              scale={1.5}
              minScale={1.5}
              onPageChanged={this.onPageAnswerChange}
              onError={(error) => console.log('error', error)}
              style={styles.pdf} />
            {maxAnswerPages > -1 && <View style={styles.paginateView}>
              <Text style={styles.pageText}>{`${currentAnswerPage}/${maxAnswerPages}`}</Text>
            </View>}
          </View>
          <View key={1} style={{ flex: 0.5 }}>
            <Pdf
              source={sourceExam}
              scale={1.5}
              minScale={1.5}
              onPageChanged={this.onPageExamChange}
              onError={(error) => console.log('error', error)}
              style={styles.pdf} />
            {maxPages > -1 && <View style={styles.paginateView}>
              <Text style={styles.pageText}>{`${currentPage}/${maxPages}`}</Text>
            </View>}
          </View>
        </ScrollView>
      </View>
    )
  }

  answerList = () => {
    const { examData, answers } = this.props.navigation.state.params
    const { disableAnswer } = this.state

    return (
      <AnswerList
        showAnswer
        disabled={disableAnswer}
        startId={examData.start_id}
        numberOfQuestion={examData.number_of_question}
        answerArr={answers}
      />
    )
  }

  render() {
    const { answerWidth } = this.state

    return (
      <View style={styles.root}>
        <StatusBar hidden />
        <View style={styles.content}>
          <View style={styles.examView}>
            {this.header()}
            {this.pdfView()}
          </View>
          <View style={[styles.answerView, { width: answerWidth }]}>
            {this.answerList()}
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1
  },
  content: {
    flex: 1,
    flexDirection: 'row'
  },
  examView: {
    flex: 1,
  },
  answerView: {
    right: 0,
    borderColor: RED_BOLD,
    borderWidth: 1
  },
  headerLandscape: {
    height: 40,
    backgroundColor: RED_BOLD,
    flexDirection: 'row',
    alignItems: 'center'
  },
  switchButtonView: {
    justifyContent: 'flex-end',
    flexDirection: 'row',
    flex: 1,
    marginRight: 10
  },
  backView: {
    marginLeft: 17
  },
  pdf: {
    flex: 1
  },
  commonView: {
    borderRadius: 4,
    backgroundColor: 'white',
    shadowColor: '#FFF',
    elevation: 1,
    shadowOffset: {
      width: 0,
      height: 2
    }
  },
  commonViewText: {
    fontSize: 13,
    fontFamily: 'OpenSans-Bold',
    color: RED_BOLD
  },
  commonViewBody: {
    flex: 1,
    marginHorizontal: 10,
    marginVertical: 12,
    justifyContent: 'center',
    alignItems: 'center',
  },
  processView: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
  },
  processText: {
    fontSize: 13,
    fontFamily: 'OpenSans-Bold',
    color: WHITE,
  },
  processContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center'
  },
  pageText: {
    fontSize: 13,
    fontFamily: 'OpenSans-Bold',
    color: 'black',
    margin: 5
  },
  paginateView: {
    position: 'absolute',
    bottom: 10,
    right: 15,
    backgroundColor: '#e6e6e6'
  }
})
