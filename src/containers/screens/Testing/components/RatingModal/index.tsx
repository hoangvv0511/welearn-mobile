import React, { PureComponent } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import Modal from 'react-native-modal';
import StarRating from 'react-native-star-rating';
import Button from '../../../../components/Button'
import { RED_BOLD, WHITE } from '../../../../../core/config/color';

interface ModalProps {
  screenId: number
  onRatePress(): void
  screenFilter: string
  name: string
  year: string
}

class RatingModal extends PureComponent<ModalProps> {

  state = {
    isVisible: false,
    starCount: 0
  }

  onStarRatingPress = (rating) => {
    this.setState({
      starCount: rating
    });
  }

  getCurrentStar = () => this.state.starCount

  open = () => this.setState({ isVisible: true })
  close = () => this.setState({ isVisible: false })

  render() {
    const { isVisible } = this.state
    const { onRatePress, name, year } = this.props

    return (
      <Modal
        isVisible={isVisible}
        backdropColor={'#757575'}
        style={styles.rootModal}
        onBackButtonPress={this.close}
        onBackdropPress={this.close}
        swipeDirection={'down'}
        onSwipeComplete={this.close}
      >
        <View style={styles.modalBodyRoot}>
          <View style={styles.content}>
            <Text style={styles.ratingText}>Bạn có thích <Text style={styles.ratingHighlight}>{name}</Text> năm <Text style={styles.ratingHighlight}>{year}</Text> này không? Hãy đưa ra đánh giá nhé</Text>
          </View>
          <View style={styles.starView}>
            <StarRating
              disabled={false}
              maxStars={5}
              fullStarColor={'orange'}
              emptyStarColor={'orange'}
              rating={this.state.starCount}
              selectedStar={(rating) => this.onStarRatingPress(rating)}
            />
          </View>
          <Button
            rootStyle={styles.ratingButton}
            text={'Rate now'}
            textStyle={styles.ratingButtonText}
            onPress={onRatePress}
          />
        </View>
      </Modal >
    )
  }
}

const styles = StyleSheet.create({
  rootModal: {
    flex: 1,
    justifyContent: 'flex-end',
    margin: 0
  },
  modalBodyRoot: {
    // flex: 0.5,
    backgroundColor: WHITE,
  },
  titleText: {
    fontSize: 14,
    fontFamily: 'Roboto-Regular',
    color: '#7F7F7F'
  },
  content: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 15,
    marginRight: 15,
    marginVertical: 10,
    flexWrap: 'wrap'
  },
  ratingText: {
    fontSize: 15,
    fontFamily: 'OpenSans-Regular',
  },
  ratingButton: {
    marginHorizontal: 15,
    backgroundColor: RED_BOLD,
    borderRadius: 5
  },
  ratingButtonText: {
    fontSize: 15,
    fontFamily: 'OpenSans-Regular',
    color: WHITE
  },
  starView: {
    marginHorizontal: 70,
    marginVertical: 16
  },
  ratingHighlight: {
    fontSize: 15,
    fontFamily: 'OpenSans-Bold',
  }
})

export default RatingModal