import React, { FunctionComponent } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { RED_BOLD, WHITE } from '../../../core/config/color'
import { TouchableOpacity } from 'react-native-gesture-handler'
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5Pro'

interface HeaderProps {
  leftView: () => React.ReactElement<any>
  middleView: () => React.ReactElement<any>
  rightView: () => React.ReactElement<any>
  back: boolean
  backCallBack(): void
  navigation: any
}

const Header: FunctionComponent<HeaderProps> = props => {

  const leftView = () => {
    const { leftView } = props;

    if (leftView) {
      return leftView
    }

    const { back, navigation } = props;
    const { toggleDrawer } = navigation;

    const _backHandler = () => {
      const { navigation, backCallBack } = props;
      const { pop } = navigation;
      if (backCallBack) {
        backCallBack();
        return
      }
      pop();
    }

    return (
      <View style={styles.leftSideRoot}>
        <TouchableOpacity
          onPress={() => (back ? _backHandler() : toggleDrawer())}
        >
          <FontAwesome5Icon
            name={back ? 'angle-left' : 'bars'}
            light
            size={back ? 35 :22}
            color={WHITE}
            style={styles.icon}
          />
        </TouchableOpacity>
      </View>

    );
  }

  const middleView = () => {
    const { middleView } = props;
    return middleView ? middleView() : null
  }

  const rightView = () => {
    const { rightView } = props;
    return rightView ? rightView() : null
  }

  return (
    <View style={styles.root}>
      <View style={styles.rootBody}>
        {leftView()}
        {middleView()}
        {rightView()}
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  root: {
    height: 60,
    backgroundColor: RED_BOLD,
    flexDirection: 'row'
  },
  rootBody: {
    flex: 1,
    flexDirection: 'row'
  },
  leftSideRoot: {
    flex: 0.15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    marginLeft: 5
  }
})

export default Header
