import PropTypes from 'prop-types';
import React, { Component } from "react";
import { StyleSheet, View, AppState } from "react-native";
import { RED_BOLD } from "../../../core/config/color";
import IconWithText from "../IconWithText";
import { formatTimer } from '../../../core/libs/formatter';
import moment from 'moment'
import 'moment/locale/vi';
moment.locale('vi');

class Timer extends Component {

  static propTypes = {
    countFrom: PropTypes.number,
    onChange: PropTypes.func,
    onFinish: PropTypes.func,
    mode: PropTypes.oneOf(['auto', 'control']),
    scale: PropTypes.number,
    direction: PropTypes.oneOf(['left', 'right', 'top', 'bottom']),
    showIcon: PropTypes.bool,
    textStyle: PropTypes.any,
    formatType: PropTypes.oneOf(['time', 'full'])
  }

  static defaultProps = {
    countFrom: 0,
    mode: 'auto',
    scale: 1,
    direction: 'bottom',
    showIcon: true,
    formatType: 'time'
  }

  constructor(props) {
    super(props)

    this.timer = setInterval(this.updateTimer, 1000);
    this.state = this.getInitialState()
  }

  getInitialState = () => {
    const initialState = {
      countFrom: Math.max(this.props.countFrom, 0),
      wentBackgroundAt: null,
      isRunning: this.props.mode === 'auto' ? true : false
    };
    return initialState;
  }

  componentDidMount() {
    AppState.addEventListener('change', this._listenForTimerChange);
  }

  componentWillUnmount() {
    clearInterval(this.timer);
    AppState.removeEventListener('change', this._listenForTimerChange);
  }

  componentDidUpdate(props, state) {
    if (this.props.countFrom !== props.countFrom) {
      this.setState({
        countFrom: Math.max(props.countFrom, 0)
      });
    }
  }


  _listenForTimerChange = currentAppState => {
    const { countFrom, wentBackgroundAt } = this.state;
    if (currentAppState === 'active' && wentBackgroundAt && this.state.isRunning) {
      const diff = (Date.now() - wentBackgroundAt) / 1000.0;
      this.setState({
        countFrom: Math.max(0, countFrom - diff)
      });
    }
    if (currentAppState === 'background') {
      this.setState({ wentBackgroundAt: Date.now() });
    }
  }

  reset = async () => {
    await this.setState(this.getInitialState())
  }

  play = async () => {
    await this.setState({ isRunning: true })
  }

  pause = async () => {
    await this.setState({ isRunning: false })
  }

  getTimeLeft = () => {
    const { formatType } = this.props
    const { countFrom } = this.state;

    if(formatType === 'full'){
      return moment(countFrom).format('LLL')
    }

    return formatTimer(countFrom)
  };

  getCountLeft = () => {
    return this.state.countFrom
  }

  updateTimer = async () => {
    const { countFrom } = this.state;

    if (!this.state.isRunning) {
      return;
    }

    if (countFrom === 0) {
      await this.setState({ countFrom: 0, isRunning: false });
      if (this.props.onFinish) {
        this.props.onFinish();
      }
      if (this.props.onChange) {
        this.props.onChange(countFrom);
      }
    } else {
      if (this.props.onChange) {
        this.props.onChange(countFrom);
      }
      this.setState({
        countFrom: Math.max(0, countFrom - 1)
      });
    }
  };

  render() {
    const { showIcon, textStyle } = this.props
    const newTime = this.getTimeLeft();

    return (
      <View style={styles.root}>
        <View style={styles.body}>
          <IconWithText
            iconName={showIcon ? 'alarm-clock' : ''}
            iconColor={RED_BOLD}
            iconSize={14}
            text={newTime}
            textStyle={[styles.text, textStyle]}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white'
  },
  text: {
    fontSize: 13,
    fontFamily: 'OpenSans-Bold',
    color: RED_BOLD
  },
  body: {
    flex: 1,
    marginHorizontal: 10,
    marginVertical: 12,
    justifyContent: 'center',
    alignItems: 'center',
  }
})
export default Timer;