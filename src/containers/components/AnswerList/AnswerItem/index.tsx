import React, { useState, FunctionComponent } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'

class AnswerItemProps {
  isCorrect: boolean = false
  isNotChoice: boolean = false
  isWrong: boolean = false
  answerMode: boolean = false
  answer: number
  index: number
  onAnswerChange: (answer: number) => void
  answerItem?: any
}

export enum Answer {
  'A' = 1,
  'B',
  'C',
  'D'
}

const AnswerItem: FunctionComponent<AnswerItemProps> = props => {
  const { answerMode, isCorrect, isNotChoice, isWrong, index, onAnswerChange, answerItem } = props
  const [currentAnswer, setCurrentAnswer] = useState(0)

  const setAnswer = (answer: number) => {
    if (currentAnswer === answer) {
      setCurrentAnswer(0)
      onAnswerChange(0)
    } else {
      setCurrentAnswer(answer)
      onAnswerChange(answer)
    }
  }

  const getAnswerColor = (ans: number) => {
    if (answerMode) {
      if (answerItem.userAnswer === 0) {
        if (ans === answerItem.correctAnswer) {
          return 'yellow'
        } else {
          return 'white'
        }
      } else if (ans === answerItem.userAnswer) {
        if (answerItem.userAnswer !== answerItem.correctAnswer) {
          return 'red'
        }
        if (answerItem.userAnswer === answerItem.correctAnswer) {
          return 'green'
        }
      } else if (answerItem.userAnswer !== answerItem.correctAnswer && ans === answerItem.correctAnswer) {
        return 'green'
      }
    } else {
      return currentAnswer === ans ? 'black' : 'white'
    }
  }

  const getTextColor = (ans: number) => {
    if ((answerMode && isCorrect || isNotChoice || isWrong) || currentAnswer === ans) {
      return 'white'
    } else {
      return 'black'
    }
  }

  return (
    <View style={styles.root}>
      <View style={styles.questionView}>
        <Text style={styles.indexText}>{`${index}.`}</Text>
      </View>
      <TouchableOpacity disabled={answerMode} style={[styles.button, styles.border, { backgroundColor: getAnswerColor(Answer.A) }]} onPress={() => setAnswer(Answer.A)}>
        <Text style={[styles.text, { color: getTextColor(Answer.A) }]}>{Answer[1]}</Text>
      </TouchableOpacity>
      <TouchableOpacity disabled={answerMode} style={[styles.button, styles.border, { backgroundColor: getAnswerColor(Answer.B) }]} onPress={() => setAnswer(Answer.B)}>
        <Text style={[styles.text, { color: getTextColor(Answer.B) }]}>{Answer[2]}</Text>
      </TouchableOpacity>
      <TouchableOpacity disabled={answerMode} style={[styles.button, styles.border, { backgroundColor: getAnswerColor(Answer.C) }]} onPress={() => setAnswer(Answer.C)}>
        <Text style={[styles.text, { color: getTextColor(Answer.C) }]}>{Answer[3]}</Text>
      </TouchableOpacity>
      <TouchableOpacity disabled={answerMode} style={[styles.button, styles.border, { backgroundColor: getAnswerColor(Answer.D) }]} onPress={() => setAnswer(Answer.D)}>
        <Text style={[styles.text, { color: getTextColor(Answer.D) }]}>{Answer[4]}</Text>
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  root: {
    height: 25,
    flexDirection: 'row',
    justifyContent: 'center',
    borderBottomColor: '#e6e6e6',
    borderBottomWidth: 1,
    marginVertical: 3
  },
  questionView: {
    width: 21,
    height: 20
  },
  button: {
    width: 20,
    height: 20,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 5
  },
  border: {
    borderColor: '#000',
    borderWidth: 1
  },
  text: {
    fontSize: 11,
    fontFamily: 'OpenSans-Regular',
  },
  indexText: {
    fontSize: 13,
    marginTop: 2,
    marginRight: 2,
    fontFamily: 'OpenSans-Regular',
  }
})

export default AnswerItem
