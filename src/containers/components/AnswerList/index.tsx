import React, { Component, PureComponent } from 'react'
import { Text, View, FlatList, StyleSheet, ScrollView } from 'react-native'
import AnswerItem from './AnswerItem'
import _ from 'lodash'

export default class AnswerList extends PureComponent {

  state = {
    answerData: []
  }

  updateAnswer = (answer, index) => {
    let { answerData } = this.state

    const answerIndex = answerData.findIndex(res => res.id === index + 1)
    if (answerIndex > -1 && answer !== 0) {
      answerData[answerIndex].answer = answer
    } else if (answerIndex > -1 && answer === 0) {
      _.pull(answerData, answerData[answerIndex])
    } else {
      answerData.push({
        id: index + this.props.startId,
        answer
      })
    }
    this.setState({ answerData })
    this.props.onAnswerChange(answerData)
  }

  renderList = ({ item, index }) => {
    const { showAnswer, answerArr } = this.props
    return (
      <AnswerItem
        answerMode={showAnswer}
        answerItem={answerArr && answerArr[index]}
        key={index}
        onAnswerChange={(ans) => this.updateAnswer(ans, index)}
        {...item}
      />
    )
  }

  introAnswer = () => {
    return (
      <View style={styles.introRoot}>
        <View style={styles.introItem}>
          <View style={[styles.introViewCircle, { backgroundColor: 'green' }]} />
          <Text style={styles.introText}>Đúng</Text>
        </View>
        <View style={styles.introItem}>
          <View style={[styles.introViewCircle, { backgroundColor: 'red' }]} />
          <Text style={styles.introText}>Sai</Text>
        </View>
        <View style={styles.introItem}>
          <View style={[styles.introViewCircle, { backgroundColor: 'yellow' }]} />
          <Text style={styles.introText}>Chưa làm (đúng)</Text>
        </View>
      </View>
    )
  }

  render() {
    const { numberOfQuestion, startId, disabled, showAnswer } = this.props
    return (
      <ScrollView pointerEvents={disabled ? 'none' : 'auto'} style={{ width: 150, opacity: disabled ? 0.5 : 1 }}>
        {showAnswer && this.introAnswer()}
        <FlatList
          data={[...Array(numberOfQuestion)].map((res, index) => {
            return {
              index: index + startId
            }
          })}
          renderItem={this.renderList}
          keyExtractor={(item, index) => index.toString()}
        />
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  introRoot: {
    width: '100%',
    height: 85
  },
  introItem: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginVertical: 5
  },
  introViewCircle: {
    width: 16,
    height: 16,
    borderRadius: 8,
    borderColor: 'black',
    borderWidth: 1,
    marginLeft: 10,
    marginRight: 10
  },
  introText: {
    fontSize: 10,
    fontFamily: 'OpenSans-Regular',
    color: 'black'
  }
})
