import * as React from 'react';
import {
  Button,
  Image,
  StyleSheet,
  Text,
  View,
  Dimensions
} from 'react-native';

const { width } = Dimensions.get('screen');

export default ({ tryAgain }) => {
  return (
    <View style={styles.root}>
      <Image style={styles.image} source={require('../../../assets/images/sources/server_error.png')} />
      <Button
        title={'Thử lại'}
        onPress={() => tryAgain && tryAgain()}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    alignItems: 'center',
    marginTop: 30
  },
  image: {
    width: width,
    height: width * (644 / 922),
    marginBottom: 30
  },
  text: {
    fontSize: 16,
    color: '#84DCF2'
  }
});
