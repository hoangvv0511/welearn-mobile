import * as React from 'react';
import { Dimensions, RefreshControl, ScrollView, StyleSheet, Text, View } from 'react-native';
import { BACKGROUND_COMMON } from '../../../core/config/color';

const { width, height } = Dimensions.get('screen');

export default class Empty extends React.Component {
  render() {
    return (
      <ScrollView
        contentContainerStyle={styles.root}
        refreshControl={this.refreshControl()}
      >
        <View style={styles.body}>
          <Text style={styles.text}>{'Chưa có dữ liệu'}</Text>
        </View>
      </ScrollView>
    );
  }

  refreshControl() {
    const { isLoading, fetchData } = this.props;
    return <RefreshControl refreshing={isLoading} onRefresh={fetchData} />;
  }
}

Empty.defaultProps = {
  isLoading: false,
  fetchData: () => { }
};

const styles = StyleSheet.create({
  root: {
    backgroundColor: BACKGROUND_COMMON,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20
  },
  body: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width,
    height: width
  },
  text: { color: '#8d8d8d', fontSize: 20, fontWeight: 'bold', marginTop: 20 },
  subText: {
    color: '#a7a7a7',
    marginTop: 10
  }
});
