import React from 'react'
import {
  Placeholder,
  PlaceholderMedia,
  PlaceholderLine,
  Fade,
  ShineOverlay
} from "rn-placeholder";
import Card from "../Card";
import { View, StyleSheet } from "react-native";

const PlaceholderBox = () => (
  <Card>
    <View style={styles.container}>
      <Placeholder
        Animation={ShineOverlay}
      >
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <PlaceholderLine width={30} />
          <PlaceholderLine width={30} />
        </View>
      </Placeholder>
      <Placeholder
        Animation={ShineOverlay}
        style={{ marginTop: 10 }}
        Left={() => <PlaceholderMedia size={50} isRound style={{ marginRight: 10, marginTop: 5 }} />}
      >
        <PlaceholderLine width={80} />
        <PlaceholderLine />
        <PlaceholderLine width={30} />
      </Placeholder>
      <Placeholder
        Animation={ShineOverlay}
        style={{ marginTop: 15 }}
      >
        <PlaceholderLine style={{ width: '100%' }} />
        <PlaceholderLine style={{ width: '100%' }} />
      </Placeholder>
    </View>
  </Card>
);

const styles = StyleSheet.create({
  container: {
    margin: 25
  }
})

export default PlaceholderBox