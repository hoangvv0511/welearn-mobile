import * as React from 'react';
import { Dimensions, FlatList, LayoutAnimation, StyleSheet, SafeAreaView, YellowBox } from 'react-native';
import { authorizedHttp } from '../../../core/services/api';
import Empty from './Empty';
import PlaceholderBox from './PlaceHolder';
import ServerError from './ServerError';
import { CancelToken } from 'axios'

const LIMIT = 10;
const OFFSET = 0;
const { height } = Dimensions.get('window');
const loadArray = [...Array(2).fill(false)];

const getFullUrl = (url, start: number, end: number) => {
  const offsetParam = `_start=${start}&_end=${end}`
  return url.indexOf('?') === -1
    ? url.concat('?', offsetParam)
    : url.concat('&', offsetParam);
}

YellowBox.ignoreWarnings(['VirtualizedLists should never be nested']);

export default class List extends React.Component {

  constructor(props) {
    super(props);
    const { url } = props;

    this.state = {
      initialLoad: true,
      loading: !!props.loading,
      data: [],
      url: url,
      error: false,
      total: -1,
      startOffSet: OFFSET,
      endOffSet: LIMIT
    };

    this.http = null;
    this.httpCancel = null;
  }

  static getDerivedStateFromProps(props, state) {
    if (props.url !== state.url) {
      return {
        data: [],
        url: props.url,
        startOffSet: OFFSET,
        endOffSet: LIMIT,
        total: -1,
        error: false,
        initialLoad: true
      }
    }
    return null
  }

  componentDidMount() {
    this.http = authorizedHttp();
    setTimeout(() => {
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
      this.setState({ initialLoad: false });
    }, 0);
    this.initialDataCall();
  }

  componentDidUpdate = () => {
    if (this.state.initialLoad) {
      setTimeout(() => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        this.setState({ initialLoad: false });
      }, 0);
      this.initialDataCall();
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return JSON.stringify(nextState) !== JSON.stringify(this.state)
  }

  componentWillUnmount() {
    this.httpCancel && this.httpCancel();
  }

  initialDataCall = () => {
    const { url, startOffSet, endOffSet } = this.state;
    const urlFull = getFullUrl(url, startOffSet, endOffSet)

    this.setState({ loading: true }, () => {
      this.apiCall(urlFull)
        .then(res => {
          this.setState({
            data: res.data,
            loading: false,
            error: false,
            total: Number.parseInt(res.headers['x-total-count'])
          });
        })
        .catch(this.handleApiError);
    });
  };

  render() {
    const { initialLoad } = this.state;
    return initialLoad ? null : this.renderList();
  }

  renderList() {
    const { renderItem } = this.props;
    const { data, loading, url, error } = this.state;
    const isPlaceholder = loading && !data.length;
    const loadingData = loadArray;

    if (error) {
      return <ServerError tryAgain={this.initialDataCall} />;
    }

    return (
      <FlatList
        style={styles.listRoot}
        keyExtractor={(item, index) => index.toString()}
        data={isPlaceholder ? loadingData : data}
        renderItem={isPlaceholder ? this.loadingItem : renderItem}
        refreshing={!isPlaceholder && loading}
        onRefresh={this.initialDataCall}
        onEndReachedThreshold={0.01}
        onEndReached={this._onEndReached}
        scrollEnabled={!isPlaceholder}
        showsVerticalScrollIndicator={false}
        ListFooterComponent={!loading ? null : this.listFooterComponent}
        ListEmptyComponent={this.emptyComponent}
      />
    );
  }

  emptyComponent = () => {
    const { loading } = this.state;
    return <Empty isLoading={loading} fetchData={this.initialDataCall} />;
  };

  renderItem = data => {
    const { item } = data;
    const { renderItem } = this.props;

    if (item) {
      return renderItem(data);
    }
  };

  loadingItem = () => {
    return <PlaceholderBox />
  };

  _onEndReached = () => {
    const { total, data, endOffSet, url } = this.state;

    if (endOffSet >= total || total < -1) {
      return
    }

    let _startOffset = endOffSet
    let _endOffset = (total - endOffSet > LIMIT) ? endOffSet + LIMIT : endOffSet + (total - endOffSet)
    const nextUrl = getFullUrl(url, _startOffset, _endOffset)

    data.length < total - 1 && this.setState({ loading: true }, () => {
      this.apiCall(nextUrl)
        .then(res => {
          this.setState({
            data: [...data, ...res.data],
            loading: false,
            endOffSet: _endOffset,
            startOffSet: _startOffset,
            total: Number.parseInt(res.headers['x-total-count'])
          })
        })
        .catch(this.handleApiError);
    })
  };

  listFooterComponent = () => {
    const { total, data } = this.state;
    return data.length <= total ? this.loadingItem() : null;
  };

  apiCall(url) {
    const { apiDataCallback } = this.props;
    return this.http
      .get(url, { cancelToken: new CancelToken(c => (this.httpCancel = c)) })
      .then((data) => {
        apiDataCallback && apiDataCallback(data);
        return data;
      });
  }

  handleApiError = e => {
    console.log(e);
    this.setState({ loading: false, error: true });
  };
}

const styles = StyleSheet.create({
  listRoot: {
    // backgroundColor: '#F4F7F8',
  },
  serverError: {},
});
