import React, { Component } from 'react'
import { StyleSheet } from 'react-native'
import CardView from 'react-native-cardview'

class Card extends Component {
  render() {
    const { rootStyle } = this.props

    return (
      <CardView
        style={[styles.cardRoot, rootStyle]}
        cardElevation={6}
        cardMaxElevation={6}
        cornerRadius={5}
        cornerOverlap={false}
      >
        {this.props.children}
      </CardView>
    )
  }
}

const styles = StyleSheet.create({
  cardRoot: {
    height: 240,
    borderRadius: 5,
    backgroundColor: 'white',
    marginHorizontal: 10,
    marginVertical: 10,
  }
})

export default Card
