import React, { FunctionComponent } from 'react'
import { View, Text, Image, ImageSourcePropType, ImageStyle, StyleSheet, Platform } from 'react-native'

interface AvatarProps {
  source: ImageSourcePropType
  imageStyle?: ImageStyle
}

const Avatar: FunctionComponent<AvatarProps> = props => {
  const { source, imageStyle } = props

  return (
    <Image
      source={source}
      style={[styles.image, imageStyle]}
    />
  )
}

const styles = StyleSheet.create({
  image: {
    width: 50,
    height: 50,
    borderRadius: Platform.OS === 'ios' ? 25 : 50
  }
})

export default Avatar
