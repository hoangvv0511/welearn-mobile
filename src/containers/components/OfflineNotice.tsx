import * as React from 'react';
import { View, Text, Dimensions, StyleSheet } from 'react-native';
const { width } = Dimensions.get('window');
function MiniOfflineSign() {
    return (
        <View style={styles.offlineContainer}>
            <Text style={styles.offlineText}>Không có kết nối Internet</Text>
        </View>
    );
}
class OfflineNotice extends React.PureComponent {
    render() {
        return <MiniOfflineSign />;
    }
}
const styles = StyleSheet.create({
    offlineContainer: {
        backgroundColor: '#b52424',
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        width,
        position: 'absolute',
        bottom: 0,
        zIndex: 1000,
    },
    offlineText: {
        color: '#fff',
    },
});
export default OfflineNotice;
