import * as React from 'react';
import { Text, TouchableOpacity, StyleSheet, ViewStyle, TextStyle } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5Pro';

interface ButtonProps {
  rootStyle: ViewStyle
  textStyle: TextStyle
  text: string
  onPress(): void
  iconName: string
  iconColor: string
  iconSize: number
  iconStyle: ViewStyle
  disabled: boolean
}

const Button: React.FunctionComponent<ButtonProps> = props => {
  const { rootStyle, textStyle, text, onPress, iconName, iconColor, iconSize, iconStyle, disabled } = props

  return (
    <TouchableOpacity disabled={disabled} activeOpacity={0.5} style={[styles.buttonRoot, rootStyle, {opacity: disabled ? 0.5 : 1}]} onPress={onPress}>
      {iconName && <Icon name={iconName} color={iconColor} size={iconSize} style={iconStyle} />}
      <Text style={[styles.buttonText, textStyle]}>{text}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  buttonRoot: {
    height: 45,
    marginTop: 10,
    marginBottom: 10,
    borderRadius: 2,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  buttonText: {
    fontSize: 14,
  },
});

export default Button
