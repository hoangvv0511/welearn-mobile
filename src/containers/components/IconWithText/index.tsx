import * as React from 'react';
import { Text, View, StyleSheet, ViewStyle, TextStyle } from 'react-native';
import FontAwesome5Pro from 'react-native-vector-icons/FontAwesome5Pro';

class IconWithTextProps {
  iconName: string
  iconColor: string
  text: string
  rootStyle: ViewStyle
  iconStyle: TextStyle
  textStyle: TextStyle
  iconSize: number
  solid?: boolean
  light?: boolean
  regular?: boolean
  numberOfLines: number = 1
}

const IconWithText: React.FunctionComponent<IconWithTextProps> = props => {
  const { rootStyle, iconName, iconColor, iconStyle, numberOfLines, text, textStyle, iconSize } = props

  return (
    <View style={[styles.root, rootStyle]}>
      {iconName ? <FontAwesome5Pro {...props} name={iconName} size={iconSize} style={iconStyle} color={iconColor} /> : null}
      <Text numberOfLines={numberOfLines} style={[styles.text, textStyle]}>{text}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  text: {
    marginLeft: 5
  }
});

export default IconWithText
