import { GoogleSignin } from '@react-native-community/google-signin';
import NetInfo from '@react-native-community/netinfo';
import * as React from 'react';
import { AppState, Platform, StatusBar, StyleSheet, UIManager, View } from 'react-native';
import Orientation from 'react-native-orientation-locker';
import { connect } from 'react-redux';
import Navigation from '../src/core/navigation';
import OfflineNotice from './containers/components/OfflineNotice';
import { webClientIdGoogle } from './core/config/key';
import { IsNetConnected } from './core/redux/action';

console.disableYellowBox = true;

class App extends React.Component {
  state = {
    isConnected: true,
    appState: AppState.currentState,
  };
  unsubscribe = null

  componentDidMount() {
    AppState.addEventListener('change', this._handleAppStateChange);
    this.unsubscribe = NetInfo.addEventListener(this.handleConnectivityChange);
    Orientation.lockToPortrait();
    this.configure()
  }

  configure = () => {
    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
    }
    GoogleSignin.configure({
      webClientId: webClientIdGoogle,
      offlineAccess: true
    });
  }

  _handleAppStateChange = (nextAppState) => {
    // if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
    //   handleUpdateApp()
    // }
    this.setState({ appState: nextAppState });
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
    this.unsubscribe()
  }

  render() {
    return (
      <View style={styles.root}>
        <StatusBar backgroundColor={'#7d0c19'} />
        <Navigation />
        {this.networkInfo()}
      </View>
    );
  }

  networkInfo() {
    const { isConnected } = this.state;
    return !isConnected ? <OfflineNotice /> : null;
  }

  handleConnectivityChange = ({ isConnected }) => {
    const { isNetConnected } = this.props;
    this.setState({ isConnected }, () => {
      isNetConnected(isConnected);
    });
  };
}

function stateToProps(state) {
  return {
    state,
  };
}

function propsToState(dispatch) {
  return {
    isNetConnected: payload => dispatch(new IsNetConnected(payload)),
  };
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  loadingScreen: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'transparent',
    zIndex: 1000,
  },
});
export default connect(
  stateToProps,
  propsToState,
)(App);
