import { createStackNavigator } from 'react-navigation-stack';
import Splash from '../../containers/screens/SplashScreen';
import { createAppContainer } from 'react-navigation';
import MainApp from './drawer'
import Exam from '../../containers/screens/Home/Exam';
import OnlineRoom from '../../containers/screens/Home/OnlineRoom';
import Home from '../../containers/screens/Home';
import Testing from '../../containers/screens/Testing';
import TestResult from '../../containers/screens/Testing/TestResult';
import TestResultDescription from '../../containers/screens/Testing/TestResultDescription';
import TestResultFullAnswer from '../../containers/screens/Testing/TestResultFullAnswer';
import History from '../../containers/screens/History';
import Profile from '../../containers/screens/UserProfile';
import SystemRoomDetail from '../../containers/screens/Home/OnlineRoom/SystemRoomDetail';

const Navigation = createStackNavigator(
  {
    Splash: Splash,
    MainApp: MainApp,
    History: History,
    Profile: Profile,
    Home: Home,
    Exam: Exam,
    OnlineRoom: OnlineRoom,
    Testing: Testing,
    TestResult: TestResult,
    TestResultDescription: TestResultDescription,
    TestResultFullAnswer: TestResultFullAnswer,
    SystemRoomDetail: SystemRoomDetail
  },
  {
    headerMode: 'none'
  },
);
export default createAppContainer(Navigation);
