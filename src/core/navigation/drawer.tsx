import * as React from 'react';
import { createDrawerNavigator } from 'react-navigation-drawer';
import Home from '../../containers/screens/Home';
import Drawer from '../../containers/screens/Drawer'

export default createDrawerNavigator(
  {
    Home: { screen: Home },
  },
  {
    contentComponent: (props, item) => {
      return <Drawer {...props} />;
    }
  }
);