import { OverflowMenuItemType } from "@ui-kitten/components";
import { TextStyle } from "react-native";

const textStyle: TextStyle = {
  fontSize: 15,
  fontFamily: 'OpenSans-Bold',
  color: 'black'
}

export const subjectMenu: OverflowMenuItemType[] = [
  {
    title: 'Tất cả',
    titleStyle: textStyle
  },
  {
    title: 'Toán học',
    titleStyle: textStyle
  },
  {
    title: 'Hóa học',
    titleStyle: textStyle
  },
  {
    title: 'Vật lý',
    titleStyle: textStyle
  },
  {
    title: 'Sinh học',
    titleStyle: textStyle
  },
  {
    title: 'Ngoại ngữ',
    titleStyle: textStyle
  },
  {
    title: 'Địa lý',
    titleStyle: textStyle
  },
  {
    title: 'Lịch sử',
    titleStyle: textStyle
  },
  {
    title: 'GDCD',
    titleStyle: textStyle
  }
]

export enum Subject {
  'toan' = 'Toán học',
  'ly' = 'Vật lý',
  'hoa' = 'Hóa học',
  'sinh' = 'Sinh học',
  'su' = 'Lịch sử',
  'dia' = 'Địa lý',
  'anh' = 'Anh văn',
  'gdcd' = 'GDCD'
}

export enum SubjectReverse {
  'toan' = 1,
  'hoa',
  'ly',
  'sinh',
  'anh',
  'dia',
  'su',
  'gdcd'
}

export const getSubjectImage = (subjectId: string) => {
  switch (subjectId) {
    case 'toan': return require('../../../assets/images/sources/toan.jpg')
    case 'ly': return require('../../../assets/images/sources/ly.jpg')
    case 'hoa': return require('../../../assets/images/sources/hoa.jpg')
    case 'sinh': return require('../../../assets/images/sources/sinh.jpg')
    case 'su': return require('../../../assets/images/sources/lichsu.jpg')
    case 'dia': return require('../../../assets/images/sources/dia.jpg')
    case 'anh': return require('../../../assets/images/sources/anh.jpg')
    case 'gdcd': return require('../../../assets/images/sources/GDCD.jpg')
  }
}

export enum ToanThematic {
  'mu_logarit' = 'Mũ - Logarit',
  'nguyen_ham_tich_phan' = 'Nguyên hàm - Tích phân',
  'so_phuc' = 'Số phức',
  'luong_giac' = 'Lượng giác',
  'day_so_cap_so' = 'Dãy số - cấp số',
  'gioi_han' = 'Giới hạn',
  'ham_so' = 'Hàm số',
  'khoi_da_dien_the_tich_khoi_da_dien' = 'Khối đa diện, thể tích khối đa diện',
  'khoi_tron_xoay_the_tich_khoi_tron_xoay' = 'Khối tròn xoay, thể tích khối tròn xoay',
  'hinh_hoc_giai_tich_oxyz' = 'Hình học giải tích Oxyz',
  'hinh_hoc_giai_tich_oxy' = 'Hình học giải tích Oxy',
  'to_hop_xac_suat' = 'Tổ hợp - xác suất',
  'phuong_trinh_he_phuong_trinh_bat_phuong_trinh' = 'Phương trình - Hệ phương trình - bất phương trình'
}

export enum LyThematic {
  'dao_dong_co' = 'Dao động cơ',
  'song_co_va_song_am' = 'Sóng cơ và sóng âm',
  'dong_dien_xoay_chieu' = 'Dòng điện xoay chiều',
  'dao_dong_va_song_dien_tu' = 'Dao động và sóng điện từ',
  'song_anh_sang' = 'Sóng ánh sáng',
  'luong_tu_anh_sang' = 'Lượng tử ánh sáng',
  'hat_nhan_nguyen_tu' = 'Hạt nhân nguyên tử',
  'dien_tich_dien_truong' = 'Điện tích – Điện trường',
  'dong_dien_khong_doi' = 'Dòng điện không đổi',
  'cam_ung_dien_tu' = 'Cảm ứng điện từ',
  'mat_va_cac_dung_cu_quang' = 'Mắt và các dụng cụ quang'
}

export enum HoaThematic {
  'su_dien_li' = 'Sự điện li',
  'phi_kim_phan_bon_hoa_hoc' = 'Phi kim - Phân bón hóa học',
  'dai_cuong_hoa_huu_co_hidrocacbon' = 'Đại cương hóa hữu cơ. Hidrocacbon',
  'ancol_phenol_andehit_axit_cacboxylic' = 'Ancol - Phenol - Andehit - Axit Cacboxylic',
  'este_lipit' = 'Este - Lipit',
  'cacbohidrat' = 'Cacbohidrat',
  'amin_amino_axit_peptit_protein' = 'Amin - Amino Axit - Peptit - Protein',
  'polime' = 'Polime',
  'dai_cuong_kim_loai' = 'Đại cương Kim loại',
  'kim_loai_IA_IIA_A1_Fe_Cr' = 'Kim loại IA, IIA, A1, Fe, Cr',
  'nhan_biet_hoa_hoc_va_hoa_hoc_voi_van_de_KT_XH_MT' = 'Nhận biết hóa học và hóa học với vấn đề Kinh tế - Xã hội - Môi trường',
}

export enum SinhThematic {
  'co_che_di_truyen_va_bien_di' = 'Cơ chế di truyền và biến dị',
  'tinh_quy_luat_cua_hien_tuong_di_truyen' = 'Tính quy luật của hiện tượng di truyền',
  'di_truyen_hoc_quan_the' = 'Di truyền học quần thể',
  'ung_dung_di_truyen_hoc_vao_chon_giong' = 'Ứng dụng di truyền học vào chọn giống',
  'di_truyen_hoc_nguoi' = 'Di truyền học người',
  'tien_hoa' = 'Tiến hóa',
  'sinh_thai_hoc' = 'Sinh thái học',
  'chuyen_hoa_vat_chat_va_nang_luong' = 'Chuyển hóa vật chất và năng lượng',
}

export enum AnhThematic {
  'phat_am' = 'Pronunciation (Phát âm)',
  'trong_am' = 'Stress (Trọng âm)',
  'ngu_phap_va_tu_vung' = 'Sentence completion (Hoàn thành câu): Ngữ pháp + Từ vựng',
  'dong_nghia' = 'Closest meaning (Đồng nghĩa)',
  'trai_nghia' = 'Opposite meaning (Trái nghĩa)',
  'cau_giao_tiep' = 'Communication (Câu giao tiếp)',
  'tim_loi_sai' = 'Error finding (Tìm lỗi sai)',
  'bien_doi_cau' = 'Sentence transformation (Biến đổi câu):Chọn câu đồng nghĩa: 3 câu',
  'ket_hop_cau' = 'Kết hợp câu: 2 câu',
  '1_doan_van' = '1 passage (1 đoạn văn)',
  '2_doan_van' = '2 passage (2 đoạn văn)',
  '1_bai_doc_8_cau' = '1 bài đọc: 8 câu',
  '1_bai_doc_5_cau' = '1 bài đọc: 5 câu'
}

export enum SuThematic {
  'CMT10_Nga_cong_cuoc_xay_dung_CNXH_o_Lien_Xo' = 'Cách mạng tháng Mười Nga năm 1917; Công cuộc xây dựng Chủ nghĩa xã hội ở Liên Xô (1921 -1941)',
  'su_hinh_thanh_trat_tu_the_gioi_sau_CTTG_thu_hai' = 'Sự hình thành trật tự thế giới mới sau Chiến tranh thế giới thứ hai(1945-1949)',
  'Lien_Xo_va_cac_nuoc_Dong_Au_Lien_Bang_Nga' = 'Liên Xô và các nước Đông Âu (1945-1991), Liên bang Nga (1991 -2000)',
  'Mi_Tay_Au_Nhat_Ban' = 'Mĩ, Tây Âu, Nhật Bản(1945-2000)',
  'quan_he_quoc_te' = 'Quan hệ quốc tế(1945-2000)',
  'Viet_Nam_tu_1858_den_1918' = 'Việt Nam từ 1858 – 1918',
  'Viet_Nam_tu_nam_1919_den_1930' = 'Việt Nam từ năm 1919 đến 1930',
  'Viet_Nam_tu_nam_1930_den_1945' = 'Việt Nam từ năm 1930 đến 1945',
  'Viet_Nam_tu_nam_1945_den_1954' = 'Việt Nam từ năm 1945 đến 1954',
  'Viet_Nam_tu_nam_1954_den_1975' = 'Việt Nam từ năm 1954 đến 1975',
  'Viet_Nam_tu_nam_1975_den_2000' = 'Việt Nam từ năm 1975 đến 2000',
}

export enum DiaThematic {
  'khai_quat_nen_kinh_te_xa_hoi_the_gioi' = 'Khái quát nền kinh tế - xã hội thế giới',
  'dia_li_khu_vuc_va_quoc_gia' = 'Địa lí Khu vực và Quốc gia',
  'dia_li_tu_nhien' = 'Địa lí tự nhiên',
  'dia_li_dan_cu' = 'Địa lí dân cư',
  'dia_li_cac_nganh_kinh_te' = 'Địa lí các ngành kinh tế',
  'dia_li_cac_vung_kinh_te' = 'Địa lí các vùng kinh tế',
  'cac_ki_nang_dia_li' = 'Các kĩ năng địa lí',
}

export enum GDCDThematic {
  'thuc_hien_phap_luat' = 'Thực hiện pháp luật',
  'cong_dan_binh_dang_truoc_phap_luat' = 'Công dân bình đẳng trước pháp luật',
  'quyen_binh_dang_cua_cong_dan_trong_mot_so_linh_vuc_cua_ĐSXH' = 'Quyền bình đẳng của công dân trong một số lĩnh vực của đời sống xã hội',
  'cong_dan_voi_cac_quyen_tu_do_co_ban' = 'Công dân với các quyền tự do cơ bản',
  'cong_dan_voi_cac_quyen_tu_do_dan_chu' = 'Công dân với các quyền dân chủ',
  'phap_luat_voi_su_phat_trien_cua_cong_dan' = 'Pháp luật với sự phát triển của công dân',
  'phap_luat_voi_su_phat_trien_ben_vung_cua_dat_nuoc' = 'Pháp luật với sự phát triển bền vững của đất nước',
  'cong_dan_voi_kinh_te' = 'Công dân với kinh tế',
}

export const getThematicEnum = (subject: string) => {
  switch (subject) {
    case 'toan': return ToanThematic
    case 'ly': return LyThematic
    case 'hoa': return HoaThematic
    case 'sinh': return SinhThematic
    case 'su': return SuThematic
    case 'dia': return DiaThematic
    case 'gdcd': return GDCDThematic
    case 'anh': return AnhThematic
    default: return null
  }
}

export enum Difficulty {
  'Nhận biết' = 1,
  'Thông hiểu',
  'Vận dụng',
  'Vận dụng cao'
}