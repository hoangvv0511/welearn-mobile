
export const getResultMessage = (point: number) => {
  if (point >= 9) return 'Xuất sắc, gần được thủ khoa rồi bạn ê 😍😍'
  else if (point < 9 && point >= 8) return 'Điểm số rất tốt. Cố gắng hơn nữa bạn nhé 😉'
  else if (point < 8 && point >= 6.5) return 'Bạn nắm tương đối tốt kiến thức, cần rèn luyện thêm !!'
  else if (point < 6.5 && point >= 5) return 'Điểm số quá thấp, cày tiếp đi bạn ơi 😑'
  else if (point < 5) return 'Quá tệ T_T. Bạn có muốn đi học nữa hay không hả ?'
  else return ''
}