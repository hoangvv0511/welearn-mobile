import ImageResizer from 'react-native-image-resizer';

const REDUCE_PERCENTAGE = 50; // It mean image aspect ratio will be reduced by 50% compared to the actual size
const QUALITY = 90
const TYPE = 'JPEG' // one of JPEG or PNG

export default function imageResizer(imageUri, width, height, imageCb, errCb) {
  const newSize = calculatorSize(width, height)

  ImageResizer.createResizedImage(imageUri, newSize.width, newSize.height, TYPE, QUALITY)
    .then((res) => {
      imageCb && imageCb(res)
    })
    .catch(err => {
      console.log(err);
      errCb && errCb(err)
    });
}

function calculatorSize(originWidth, originHeight) {
  const percentToValue = REDUCE_PERCENTAGE / 100

  return {
    width: originWidth * percentToValue,
    height: originHeight * percentToValue
  }
}