export const formatSizeUnit = (bytes: number) => {
  if (bytes >= 1073741824) { bytes = (bytes / 1073741824).toFixed(2) + " GB"; }
  else if (bytes >= 1048576) { bytes = (bytes / 1048576).toFixed(2) + " MB"; }
  else if (bytes >= 1024) { bytes = (bytes / 1024).toFixed(2) + " KB"; }
  else if (bytes > 1) { bytes = bytes + " bytes"; }
  else if (bytes == 1) { bytes = bytes + " byte"; }
  else { bytes = "0 bytes"; }
  return bytes;
};

export const formatTimer = (secs) => {
  var timeInSecond = parseInt(secs, 10)
  var hours = Math.floor(timeInSecond / 3600) % 24
  var minutes = Math.floor(timeInSecond / 60) % 60
  var second = timeInSecond % 60
  return [hours, minutes, second]
    .map(v => v < 10 ? "0" + v : v)
    .filter((v, i) => v !== "00" || i > 0)
    .join(":")
}