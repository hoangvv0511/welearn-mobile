import { Action } from 'redux';

export const SET_USER = 'SET_USER';
export const IS_NET_CONNECTED = 'IS_NET_CONNECTED';

export class UserSetAction implements Action {
    readonly type: string = SET_USER;
    constructor(public payload: any) {
        return Object.assign({}, this);
    }
}

export class IsNetConnected implements Action {
    readonly type: string = IS_NET_CONNECTED;
    constructor(public payload: boolean) {
      return Object.assign({}, this);
    }
  }

export type Action = UserSetAction
