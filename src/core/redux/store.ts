import { createStore, applyMiddleware, combineReducers } from 'redux';
import { enableBatching, batchDispatchMiddleware } from 'redux-batched-actions';
import thunk from 'redux-thunk';
import System from './reducer';

const reducers = combineReducers({
  System,
});

export default createStore(
  enableBatching(reducers),
  applyMiddleware(thunk, batchDispatchMiddleware)
);
