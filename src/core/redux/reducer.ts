import * as System from './action';

const initialState = {
  user: {},
  isNetConnected: true
};

export default function userReducer(
  state = initialState,
  action: System.Action,
) {
  switch (action.type) {
    case System.SET_USER:
      return {
        ...state,
        user: action.payload,
      };
    case System.IS_NET_CONNECTED:
      return {
        ...state,
        isNetConnected: action.payload
      };
    default:
      return {
        ...state,
      };
  }
}
