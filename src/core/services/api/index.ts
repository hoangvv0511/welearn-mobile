import axios from 'axios';
import { BASE_URL } from './config';

export const http = (() => {
  const httpClient = axios.create({
    baseURL: BASE_URL,
    timeout: 30000
  });
  return httpClient;
})();

export const authorizedHttp = () => {
  http.defaults.timeout = 100000;
  return http;
};